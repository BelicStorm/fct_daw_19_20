# <Nombre> 

Control y gestion del horario de trabajo con las ultimas tecnologias en desarrollo web. 

## Construido con 🛠️
* [React](https://es.reactjs.org/) - El framework web usado
    * [Redux](https://es.redux.js.org/) 
    * [Hooks](https://es.reactjs.org/docs/hooks-intro.html)
    * [Next.Js]()
* [Node,Js]() - Tecnologia de backend
    * [Express]()
    * [Typescript]()
    * [TypeOrm]()
    * [GraphQL]()
    * [ApolloFederation]()
* [Postgres](https://www.postgresql.org/) - Tecnologia de base de datos

### Pre-requisitos 📋
NodeJs para utilizar npm o en otro orden de cosas sustituyelo por el gestor que prefieras.
Docker y docker-compose para poder instalar nuestro Backend:
* [Docker en ubuntu](https://www.digitalocean.com/community/tutorials/como-instalar-y-usar-docker-en-ubuntu-18-04-1-es)
* [Docker en Windows](https://docs.docker.com/docker-for-windows/install/)
* [Docker-Compose](https://www.digitalocean.com/community/tutorials/como-instalar-docker-compose-en-ubuntu-18-04-es)

### Instalación 🔧
Antes de todo clonaremos el repositorio. Ya clonado instalaremos los apartados en el siguiente orden:
* Backend
```
cd backend
npm install // yarn install
docker-compose up --build
```
* Frontend React ---under construction
```
cd frontend
npm install // yarn install
(Ya instalado)
npm run start // yarn serve
```

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto:
1. [Backend]() ---under construction
2. [Frontend React]() ---under construction
3. [Demos](Examples/Readme.md)



