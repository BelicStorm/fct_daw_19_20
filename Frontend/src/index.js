import React from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import { ApolloProvider } from 'react-apollo';
import {HashRouter as Router} from 'react-router-dom'

import store from "./duck/store"
import AppLayout from './components/layout/layout.component'
import 'antd/dist/antd.dark.css';
import {client,BaseRouter} from "./router"


/* Index será la base de nuestra aplicación. El Provider juntará nuestros componentes con el Store de React-Redux */
ReactDOM.render(
    <Provider store={store}>{/* Hace posible el uso del objeto store y los metodos de Redux */}
        <ApolloProvider client={client}>{/* Hace posible el uso del objeto client en nuestos componentes */}
            <Router>
                <AppLayout>{/* Pinta los componentes que carga BaseRouter */}
                    <BaseRouter/>{/* Se encarga de cargar los componentes dependiendo de la ruta en la que nos encontremos */}
                </AppLayout>
            </Router>
        </ApolloProvider>
    </Provider>
        ,
    document.getElementById('root')
)