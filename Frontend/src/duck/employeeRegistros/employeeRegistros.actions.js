import {CREATE_REGISTRO_TABLE,FETCH_REGISTRO,INSERT_REGISTRO} from "./employeeRegistros.types"
import {queries} from "../../router/index"

const CREATE_REGISTRO_TABLE_ACTIONS = {
    CREATE_REGISTRO_TABLE:(variables)=>{
        return {type: CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE, method:"mutation",query:queries.employeeRegistros.create_table, 
                variables:variables }
    },
    CREATE_REGISTRO_TABLE_PENDING:()=>{
        return {type: CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE_PENDING}
    },
    CREATE_REGISTRO_TABLE_SUCCESS:()=>{
        return {type: CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE_SUCCESS}
    },
    CREATE_REGISTRO_TABLE_FAILURE:()=>{
        return {type: CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE_FAILURE}
    },
    
}
const FETCH_REGISTRO_ACTIONS = {
    FETCH_REGISTRO:(variables)=>{
        return {type: FETCH_REGISTRO.FETCH_REGISTRO, method:"query",query:queries.employeeRegistros.fetch_registro, 
                variables:variables }
    },
    FETCH_ALL_REGISTROS:(variables)=>{
        return {type: FETCH_REGISTRO.FETCH_REGISTRO, method:"query",query:queries.employeeRegistros.fetch_all_registros, 
                variables:variables }
    },
    FETCH_REGISTRO_PENDING:()=>{
        return {type: FETCH_REGISTRO.FETCH_REGISTRO_PENDING}
    },
    FETCH_REGISTRO_SUCCESS:()=>{
        return {type: FETCH_REGISTRO.FETCH_REGISTRO_SUCCESS}
    },
    FETCH_REGISTRO_FAILURE:()=>{
        return {type: FETCH_REGISTRO.FETCH_REGISTRO_FAILURE}
    }
}

const INSERT_REGISTRO_ACTIONS = {
    INSERT_REGISTRO:(variables)=>{
        return {type: INSERT_REGISTRO.INSERT_REGISTRO, method:"mutation",query:queries.employeeRegistros.insert_registro, 
                variables:variables }
    },
    INSERT_REGISTRO_PENDING:()=>{
        return {type: INSERT_REGISTRO.INSERT_REGISTRO_PENDING}
    },
    INSERT_REGISTRO_SUCCESS:()=>{
        return {type: INSERT_REGISTRO.INSERT_REGISTRO_SUCCESS}
    },
    INSERT_REGISTRO_FAILURE:()=>{
        return {type: INSERT_REGISTRO.INSERT_REGISTRO_FAILURE}
    }
}

export {
    CREATE_REGISTRO_TABLE_ACTIONS,FETCH_REGISTRO_ACTIONS,INSERT_REGISTRO_ACTIONS
}
