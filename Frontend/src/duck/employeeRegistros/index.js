import {CREATE_REGISTRO_TABLE_ACTIONS,FETCH_REGISTRO_ACTIONS,INSERT_REGISTRO_ACTIONS} from "./employeeRegistros.actions"
import employeeRegistros_reducer from "./employeeRegistros.reducer"



export { CREATE_REGISTRO_TABLE_ACTIONS,FETCH_REGISTRO_ACTIONS,INSERT_REGISTRO_ACTIONS,
        employeeRegistros_reducer}