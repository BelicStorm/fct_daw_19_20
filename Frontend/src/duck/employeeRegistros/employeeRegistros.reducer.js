import {CREATE_REGISTRO_TABLE,FETCH_REGISTRO,INSERT_REGISTRO} from "./employeeRegistros.types"


const employeeRegistros_state = {
    EmRe_loading:false,
    EmRe_error:false,
    EmRe_error_data:"",
    EmRe_data:{},
}


const employeeRegistros_reducer = ( state = employeeRegistros_state, action ) => {
    const reducer_cases = {
        //Async starters
        [CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE]:{...state, EmRe_loading:true},
        [FETCH_REGISTRO.FETCH_REGISTRO]:{...state, EmRe_loading:true},
        [INSERT_REGISTRO.INSERT_REGISTRO_PENDING]:{...state, EmRe_loading:true},

        //Async steps
        [CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE_PENDING]:{...state, EmRe_loading:true, EmRe_error:false,EmRe_error_data:"", EmRe_data:{}},
        [CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE_SUCCESS]:{...state, EmRe_loading:false, EmRe_error:false,EmRe_error_data:"", EmRe_data:action.data},
        [CREATE_REGISTRO_TABLE.CREATE_REGISTRO_TABLE_FAILURE]:{...state, EmRe_loading:false, EmRe_error:true, EmRe_error_data:action.error, EmRe_data:{}},
        [FETCH_REGISTRO.FETCH_REGISTRO_PENDING]:{...state, EmRe_loading:true, EmRe_error:false,EmRe_error_data:"", EmRe_data:{}},
        [FETCH_REGISTRO.FETCH_REGISTRO_SUCCESS]:{...state, EmRe_loading:false, EmRe_error:false,EmRe_error_data:"", EmRe_data:action.data},
        [FETCH_REGISTRO.FETCH_REGISTRO_FAILURE]:{...state, EmRe_loading:false, EmRe_error:true, EmRe_error_data:action.error, EmRe_data:{}},
        [INSERT_REGISTRO.INSERT_REGISTRO_PENDING]:{...state, EmRe_loading:true, EmRe_error:false,EmRe_error_data:"", EmRe_data:{}},
        [INSERT_REGISTRO.INSERT_REGISTRO_SUCCESS]:{...state, EmRe_loading:false, EmRe_error:false,EmRe_error_data:"", EmRe_data:action.data},
        [INSERT_REGISTRO.INSERT_REGISTRO_FAILURE]:{...state, EmRe_loading:false, EmRe_error:true, EmRe_error_data:action.error, EmRe_data:{}}
    }
    return reducer_cases[action.type]===undefined ? state : reducer_cases[action.type]
}

export default employeeRegistros_reducer