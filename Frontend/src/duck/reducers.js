import { combineReducers } from "redux";
import {reducers} from "./index"

const reducer = combineReducers( {
    AUTH_REDUCER: reducers.auth_reducer,
    MANAGE_EMPLOYEES_REDUCER:reducers.manage_employees_reducer,
    MANAGE_PLANS_REDUCER:reducers.manage_plans_reducer,
    EMPLOYEE_REGISTROS_REDUCER:reducers.employeeRegistros_reducer
} );

export default reducer;