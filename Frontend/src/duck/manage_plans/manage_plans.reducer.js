import {INSERT_PLAN,PLANS_FETCH} from "./manage_plans.types"


const manage_plans_state = {
    plans_loading:false,
    plans_error:false,
    plans_error_data:"",
    plans_data:{},
}


const manage_plans_reducer = ( state = manage_plans_state, action ) => {
    const reducer_cases = {
        //Async starters
        [INSERT_PLAN.INSERT_PLAN]:{...state, plans_loading:true},
        [PLANS_FETCH.SELECT_PLANS_FETCH]:{...state, plans_loading:true},

        //Async steps
        [INSERT_PLAN.INSERT_PLAN_PENDING]:{...state, plans_loading:true, plans_error:false,plans_error_data:"", plans_data:{}},
        [INSERT_PLAN.INSERT_PLAN_SUCCESS]:{...state, plans_loading:false, plans_error:false,plans_error_data:"", plans_data:action.data},
        [INSERT_PLAN.INSERT_PLAN_FAILURE]:{...state, plans_loading:false, plans_error:true, plans_error_data:action.error, plans_data:{}},

        [PLANS_FETCH.SELECT_PLANS_FETCH_PENDING]:{...state, plans_loading:true, plans_error:false,plans_error_data:"", plans_data:{}},
        [PLANS_FETCH.SELECT_PLANS_FETCH_SUCCESS]:{...state, plans_loading:false, plans_error:false,plans_error_data:"", plans_data:action.data},
        [PLANS_FETCH.SELECT_PLANS_FETCH_FAILURE]:{...state, plans_loading:false, plans_error:true, plans_error_data:action.error, plans_data:{}},
    }
    return reducer_cases[action.type]===undefined ? state : reducer_cases[action.type]
}

export default manage_plans_reducer