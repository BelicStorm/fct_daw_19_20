import {INSERT_PLAN,PLANS_FETCH} from "./manage_plans.types"
import {queries} from "../../router/index"

const INSERT_PLAN_ACTIONS = {
    INSERT_PLAN:(variables)=>{
        return {type: INSERT_PLAN.INSERT_PLAN, method:"mutation",query:queries.manage_plans.insert_plan, 
        variables:variables.plan_name!=="Base" ? variables 
                 : {plan_name:"Base",horas_dia:0,vacaciones:0,hora_entrada:"0",hora_salida:"0",empresa:variables.empresa} }
    },
    INSERT_PLAN_PENDING:()=>{
        return {type: INSERT_PLAN.INSERT_PLAN_PENDING}
    },
    INSERT_PLAN_SUCCESS:()=>{
        return {type: INSERT_PLAN.INSERT_PLAN_SUCCESS}
    },
    INSERT_PLAN_FAILURE:()=>{
        return {type: INSERT_PLAN.INSERT_PLAN_FAILURE}
    },
    
}
const PLANS_FETCH_ACTIONS = {
    SELECT_ALL_PLANS_FETCH:()=>{
        return {type: PLANS_FETCH.SELECT_PLANS_FETCH, method:"query",query:queries.manage_plans.get_all_plans}
    },
    SELECT_PLAN_BY_ID:(variables)=>{
        return {type: PLANS_FETCH.SELECT_PLANS_FETCH, method:"query",query:queries.manage_plans.getPlanById, variables:variables}
    },
    SELECT_PLANS_FETCH_PENDING:()=>{
        return {type: PLANS_FETCH.SELECT_PLANS_FETCH_PENDING}
    },
    SELECT_PLANS_FETCH_SUCCESS:()=>{
        return {type: PLANS_FETCH.SELECT_PLANS_FETCH_SUCCESS}
    },
    SELECT_PLANS_FETCH_FAILURE:()=>{
        return {type: PLANS_FETCH.SELECT_PLANS_FETCH_FAILURE}
    },
}
export {
    INSERT_PLAN_ACTIONS,PLANS_FETCH_ACTIONS
}
