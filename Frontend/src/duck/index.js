import {LOGIN_ACTIONS,LOGOUT_ACTIONS,REGISTER_COMPANY_ACTIONS,REGISTER_NEW_EMPLOYEE_ACTIONS,auth_reducer} from "./authDuck/index"
import {FETCH_USERS_ACTIONS,manage_employees_reducer} from "./manage_employees/index"
import {INSERT_PLAN_ACTIONS,PLANS_FETCH_ACTIONS,manage_plans_reducer} from "./manage_plans/index"
import {CREATE_REGISTRO_TABLE_ACTIONS,FETCH_REGISTRO_ACTIONS,INSERT_REGISTRO_ACTIONS,employeeRegistros_reducer} from "./employeeRegistros/index"

let actions = {
    authActions: {
        LOGIN_ACTIONS:LOGIN_ACTIONS,
        LOGOUT_ACTIONS:LOGOUT_ACTIONS,
        REGISTER_COMPANY_ACTIONS:REGISTER_COMPANY_ACTIONS,
        REGISTER_NEW_EMPLOYEE_ACTIONS:REGISTER_NEW_EMPLOYEE_ACTIONS
    },
    manageEmployeesActions:{
        FETCH_USERS_ACTIONS:FETCH_USERS_ACTIONS
    },
    managePlansActions:{
        INSERT_PLAN_ACTIONS:INSERT_PLAN_ACTIONS,
        PLANS_FETCH_ACTIONS:PLANS_FETCH_ACTIONS
    },
    employeeRegistrosActions:{
        CREATE_REGISTRO_TABLE_ACTIONS:CREATE_REGISTRO_TABLE_ACTIONS,
        FETCH_REGISTRO_ACTIONS:FETCH_REGISTRO_ACTIONS,
        INSERT_REGISTRO_ACTIONS:INSERT_REGISTRO_ACTIONS
    }
}
const reducers = {
    auth_reducer:auth_reducer,
    manage_employees_reducer:manage_employees_reducer,
    manage_plans_reducer:manage_plans_reducer,
    employeeRegistros_reducer:employeeRegistros_reducer
}

export {
    reducers, actions
}