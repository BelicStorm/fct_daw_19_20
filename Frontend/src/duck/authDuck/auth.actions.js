import {login,logout,registerCompany,registerNewEmployee} from "./auth.types"
import {queries} from "../../router/index"

const REGISTER_COMPANY_ACTIONS = {
    REGISTERCOMPANY:(variables)=>{
        return {type: registerCompany.REGISTERCOMPANY, method:"mutation",query:queries.auth.CreateCompany, variables:variables }
    },
    CREATE_USER_TABLE:(variables)=>{
        return {type: registerCompany.CREATE_USER_TABLE, method:"mutation",query:queries.auth.CreateUserTable, variables:variables }
    },
    CREATE_PlAN_TABLE:(variables)=>{
        return {type: registerCompany.CREATE_PLAN_TABLE, method:"mutation",query:queries.auth.CreatePlanTable, variables:variables }
    },

    REGISTERCOMPANY_PENDING:()=>{
        return {type: registerCompany.REGISTERCOMPANY_PENDING,}
    },
    REGISTERCOMPANY_SUCCESS:()=>{
        return {type: registerCompany.REGISTERCOMPANY_SUCCESS,}
    },
    REGISTERCOMPANY_FAILURE:()=>{
        return {type: registerCompany.REGISTERCOMPANY_FAILURE,}
    },
    
    CREATE_USER_TABLE_PENDING:()=>{
        return {type: registerCompany.CREATE_USER_TABLE_PENDING,}
    },
    CREATE_USER_TABLE_SUCCESS:()=>{
        return {type: registerCompany.CREATE_USER_TABLE_SUCCESS }
    },
    CREATE_USER_TABLE_FAILURE:()=>{
        return {type: registerCompany.CREATE_USER_TABLE_FAILURE }
    },

    CREATE_PLAN_TABLE_PENDING:()=>{
        return {type: registerCompany.CREATE_PLAN_TABLE_PENDING,}
    },
    CREATE_PLAN_TABLE_SUCCESS:()=>{
        return {type: registerCompany.CREATE_PLAN_TABLE_SUCCESS }
    },
    CREATE_PLAN_TABLE_FAILURE:()=>{
        return {type: registerCompany.CREATE_PLAN_TABLE_FAILURE }
    },


}
const REGISTER_NEW_EMPLOYEE_ACTIONS = {
    REGISTER_NEW_EMPLOYEE:(variables)=>{
        return {type: registerNewEmployee.REGISTERNEWEMPLOYEE, method:"mutation",query:queries.auth.insertUser, 
                variables:variables.empresa_name 
                ? {...variables, plan_id:1}  //insert the Boss user
                : {...variables, empresa_name:localStorage.getItem("user_Empresa")} }//Insert an employee user
    },
    REGISTER_NEW_EMPLOYEE_PENDING:()=>{
        return {type: registerNewEmployee.REGISTERNEWEMPLOYEE_PENDING }
    },
    REGISTER_NEW_EMPLOYEE_SUCCESS:()=>{
        return {type: registerNewEmployee.REGISTERNEWEMPLOYEE_SUCCESS }
    },
    REGISTER_NEW_EMPLOYEE_FAILURE:()=>{
        return {type: registerNewEmployee.REGISTERNEWEMPLOYEE_FAILURE }
    }
}
const LOGIN_ACTIONS = {
    LOGIN:(variables)=>{
        return {type: login.LOGIN, method:"query",query:queries.auth.login, variables:variables }
    },
    LOGIN_SUCCESS:()=>{
        return {type: login.LOGIN_SUCCESS }
    },
    LOGIN_PENDING:()=>{
        return {type: login.LOGIN_PENDING }
    },
    LOGIN_FAILURE:()=>{
        return {type: login.LOGIN_FAILURE}
    },
}
const LOGOUT_ACTIONS = {
    LOGOUT:()=>{
        window.localStorage.clear()
        return {type: logout.LOGOUT}
    }
}
export {
    LOGIN_ACTIONS,LOGOUT_ACTIONS,REGISTER_COMPANY_ACTIONS,REGISTER_NEW_EMPLOYEE_ACTIONS
}
