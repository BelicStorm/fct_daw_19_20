import {login, logout, registerCompany,registerNewEmployee} from "./auth.types"


const auth_state = {
    auth_loading:false,
    auth_next_step:"",
    auth_error:false,
    auth_error_data:null,
    auth_data:{},
    currentUser:window.localStorage.getItem('user_email')!==null ? window.localStorage.getItem('user_email') : null,
    currentEmpresa:window.localStorage.getItem('user_Empresa')!==null ? window.localStorage.getItem('user_Empresa') : null,
    token:window.localStorage.getItem('token')!==null ? window.localStorage.getItem('token') : null,
    user_type:window.localStorage.getItem('user_type')!==null ? window.localStorage.getItem('user_type') : null,
    user_plan:window.localStorage.getItem('plan_id')!==null ? window.localStorage.getItem('plan_id') : null,
}


const auth_reducer = ( state = auth_state, action ) => {
    const reducer_cases = {
        //Async starters
        [login.LOGIN]:{...state, auth_loading:true },
        [registerCompany.REGISTERCOMPANY]:{...state, auth_loading:true},
        [registerCompany.CREATE_USER_TABLE]:{...state, auth_loading:true},
        [registerCompany.CREATE_USER_TABLE]:{...state, auth_loading:true},
        [registerNewEmployee.REGISTERNEWEMPLOYEE]:{...state, auth_loading:true},
        
        //async steps
        [login.LOGIN_PENDING]:{...state, auth_loading:true,  auth_data:"",auth_error:false, auth_error_data:null },
        [login.LOGIN_SUCCESS]:{...state, auth_loading:false, auth_data:action.data!==undefined ? action.data.login : ""},
        [login.LOGIN_FAILURE]:{...state, auth_loading:false, auth_error:true, auth_error_data:action.error },

        [registerNewEmployee.REGISTERNEWEMPLOYEE_PENDING]:{...state, auth_loading:true,  auth_data:"",auth_error:false, auth_error_data:null },
        [registerNewEmployee.REGISTERNEWEMPLOYEE_SUCCESS]:{...state, auth_loading:false, auth_next_step:"",
                                                                     auth_data:action.data!==undefined ? action.data : ""},
        [registerNewEmployee.REGISTERNEWEMPLOYEE_FAILURE]:{...state, auth_loading:false, auth_next_step:"", auth_error:true, auth_error_data:action.error },
        
        [registerCompany.REGISTERCOMPANY_PENDING]:{...state, auth_loading:true,auth_data:"",auth_error:false, auth_error_data:null },
        [registerCompany.REGISTERCOMPANY_SUCCESS]:{...state, auth_loading:true, auth_next_step:"createPlanTable",
                                                             auth_data:action.data!==undefined ? action.data.registerNewEmpresa : ""},
        [registerCompany.REGISTERCOMPANY_FAILURE]:{...state, auth_loading:false, auth_next_step:"", auth_error:true, auth_error_data:action.error },

        [registerCompany.CREATE_USER_TABLE_PENDING]:{...state, auth_loading:true,auth_data:"",auth_error:false, auth_error_data:null},
        [registerCompany.CREATE_USER_TABLE_SUCCESS]:{...state, auth_loading:true, auth_next_step:"insertUser",
                                                             auth_data:action.data!==undefined ? action.data : ""},
        [registerCompany.CREATE_USER_TABLE_FAILURE]:{...state, auth_loading:false, auth_next_step:"", auth_error:true, auth_error_data:action.error },

        [registerCompany.CREATE_PLAN_TABLE_PENDING]:{...state, auth_loading:true,auth_data:"",auth_error:false, auth_error_data:null },
        [registerCompany.CREATE_PLAN_TABLE_SUCCESS]:{...state, auth_loading:true, auth_next_step:"createTable",
                                                             auth_data:action.data!==undefined ? action.data : ""},
        [registerCompany.CREATE_PLAN_TABLE_FAILURE]:{...state, auth_loading:false, auth_next_step:"", auth_error:true, auth_error_data:action.error },

        //Sync states
        [logout.LOGOUT]:{...state, currentUser: null,currentEmpresa: null,token: null, user_type:null, user_plan:null},
        
    }
    return reducer_cases[action.type]===undefined ? state : reducer_cases[action.type]
}

export default auth_reducer