import {FETCH_USERS} from "./manage_employees.types"
import {queries} from "../../router/index"

const FETCH_USERS_ACTIONS = {
    FETCH_USERS:()=>{
        return {type: FETCH_USERS.FETCH_USERS, method:"query",query:queries.manage_employees.select_all }
    },
    FETCH_USERS_BY_MAIL:(variables)=>{
        return {type: FETCH_USERS.FETCH_USERS, method:"query",query:queries.manage_employees.select_by_mail, variables:variables }
    },
    FETCH_USERS_PENDING:()=>{
        return {type: FETCH_USERS.FETCH_USERS_PENDING}
    },
    FETCH_USERS_SUCCESS:()=>{
        return {type: FETCH_USERS.FETCH_USERS_SUCCESS}
    },
    FETCH_USERS_FAILURE:()=>{
        return {type: FETCH_USERS.FETCH_USERS_FAILURE}
    },
    
}

export {
    FETCH_USERS_ACTIONS
}
