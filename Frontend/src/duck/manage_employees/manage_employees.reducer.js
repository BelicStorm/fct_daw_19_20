import {FETCH_USERS} from "./manage_employees.types"


const manage_employees_state = {
    MaEm_loading:false,
    MaEm_error:false,
    MaEm_error_data:"",
    MaEm_data:{},
}


const manage_employees_reducer = ( state = manage_employees_state, action ) => {
    const reducer_cases = {
        //Async starters
        [FETCH_USERS.FETCH_USERS]:{...state, MaEm_loading:true},

        //Async steps
        [FETCH_USERS.FETCH_USERS_PENDING]:{...state, MaEm_loading:true, MaEm_error:false,MaEm_error_data:"", MaEm_data:{}},
        [FETCH_USERS.FETCH_USERS_SUCCESS]:{...state, MaEm_loading:false, MaEm_error:false,MaEm_error_data:"", MaEm_data:action.data},
        [FETCH_USERS.FETCH_USERS_FAILURE]:{...state, MaEm_loading:false, MaEm_error:true, MaEm_error_data:action.error, MaEm_data:{}}
    }
    return reducer_cases[action.type]===undefined ? state : reducer_cases[action.type]
}

export default manage_employees_reducer