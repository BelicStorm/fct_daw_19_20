import {agent} from "../router/index"
const duckOperations = store => next => action => {
    console.log(action);
    
    if(action.method !== undefined){    /* Si la accion tiene un metodo asignado llamaremos al servidor */
          store.dispatch({type:action.type+"_PENDING"})
      agent[action.method](action.variables,action.query).then(
        res=>{
          /* Si la peticion ha sido correcta lanzaremos la accion anterior pero ahora con el sufijo de _SUCCESS */

          if(res.data.login!==undefined){
            window.localStorage.setItem('token', res.data.login.token);
            window.localStorage.setItem('user_email', action.variables.email );
            window.localStorage.setItem('user_type', res.data.login.user_type );
            window.localStorage.setItem('user_Empresa', action.variables.empresa );
            window.localStorage.setItem('plan_id', res.data.login.plan_id );
          }
          store.dispatch({type:action.type+"_SUCCESS",data:res.data}) 
        },
        err=>{
          /* Si la peticion ha sido erronea lanzaremos la accion anterior pero ahora con el sufijo de _FAILURE */
          store.dispatch({type:action.type+"_FAILURE",error:err})
        })
        return;
    }
    /* Si la accion no necesita de una logica en el servidor la pasaremos directamente al REDUCER */
    next(action)

 }
export default duckOperations