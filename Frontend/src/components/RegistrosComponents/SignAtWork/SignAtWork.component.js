import React from "react"
import {connect} from 'react-redux'
import {actions} from "../../../duck/index"
import { Button,/* Popconfirm, message, Progress */ } from 'antd';

const mapStateToProps = (state) => {
    return {
        //registros state
        EmRe_loading:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_loading,
        EmRe_error:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error,
        EmRe_error_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error_data,
        EmRe_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_data,
        //Auth state
        currentEmpresa:state.AUTH_REDUCER.currentEmpresa,
        currentUser:state.AUTH_REDUCER.currentUser,
        user_plan:state.AUTH_REDUCER.user_plan,
    }
}
const mapDispatchToProps = dispatch =>({ /* Creamos una funcion que contenga todas las acciones 
    necesarias que modifiquen el store a nuestro placer. En este caso llamaremos a las acciones del pato
    asociado a esta funcionalidad, el cual se encargara de efectuar toda la logica de la peticion al servidor y de lanzar
    el action que modificara el reducer */
    signUp:(variables)=>dispatch(actions.employeeRegistrosActions.INSERT_REGISTRO_ACTIONS.INSERT_REGISTRO(variables))
})

export class SignAtWork extends React.Component {
    constructor(props){
        super(props)
        this.state={
            inWork:localStorage.getItem("inWork")==="true"? true : false,
        }
        this.sign=this.sign.bind(this)
    }
    sign(work){/* Formateamos los datos para insertar el registro */
        const today = new Date()        
        let registro = {
            id_plan:parseInt(this.props.user_plan),
            Fecha:`${today.getDate()}/${today.getMonth()+1}/${today.getUTCFullYear()}`,
            hora:`${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`,
            entrada:work,
            salida:!work,
            Ausencia:false,
            AusenciaConfirmada:false,
            Vacaciones:false,
            VacacionesConfirmadas:false,
            user:this.props.currentUser
        }
        this.props.signUp(registro) /* Insertamos el registro */
        this.setState({...this.state, inWork:work})/* Cambiamos el estado de los botones */
        if(work===true){localStorage.setItem("inWork",true)}else{localStorage.removeItem("inWork")}
    }
    shouldComponentUpdate(nextProps, nextState){/* ReRenderizamos el componente dependiendo de los cambios de estado */
        if(nextProps.EmRe_loading !== this.props.EmRe_loading){
            return true
        }
        if(this.state.inWork!== nextState.inWork){
            return true
        }
        return false
    }
    render(){
        return (
           <React.Fragment>
                <Button style={{ width: "100%" }} disabled={this.state.inWork}  onClick={()=>this.sign(true)}>Entrar</Button> 
                <Button style={{ width: "100%" }} disabled={!this.state.inWork} onClick={()=>this.sign(false)}>Salir</Button>
           </React.Fragment>
        )
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (SignAtWork)




