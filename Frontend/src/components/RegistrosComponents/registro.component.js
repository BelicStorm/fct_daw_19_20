import React from "react"
import {connect} from 'react-redux'
import {actions} from "../../duck/index"
import { Table,Typography } from 'antd';
import {CheckCircleTwoTone,CloseCircleTwoTone } from "@ant-design/icons"
const { Text } = Typography;

const mapStateToProps = (state) => {
    return {
        //auth state
        currentEmpresa:state.AUTH_REDUCER.currentEmpresa,
        //registros state
        EmRe_loading:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_loading,
        EmRe_error:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error,
        EmRe_error_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error_data,
        EmRe_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_data,
    }
}
const mapDispatchToProps = dispatch =>({ 
    getRegistro:(variables)=>dispatch(actions.employeeRegistrosActions.FETCH_REGISTRO_ACTIONS.FETCH_REGISTRO(variables)),
})
/* Mostramos todas los registros que pertenecen al dia que seleccionamos e indicamos si
    ha cumplido con las horas por dia que indican el plan al que pertenece el usuario */
const expandedRowRender = (table_data) => {
    const columns = [ /* Generamos las columnas de la tabla */
      { title: 'Hora', dataIndex: 'hora', key: 'hora' },
      { title: 'Entrada', dataIndex: 'entrada', key: 'entrada' },
      { title: 'Salida', dataIndex: 'salida', key: 'salida' },
      { title: 'Ausencia', dataIndex: 'asuencia', key: 'asuencia' },
      { title: 'Vacaciones', dataIndex: 'vacaciones', key: 'vacaciones' },
      { title: 'Ausencia Confirmada', dataIndex: 'auco', key: 'auco' },
      { title: 'Vacaciones Confirmadas', dataIndex: 'vaco', key: 'vaco' },
    ];
    let numbers = [];
    let total_worked=false
    const data = [];
    /* generamos las filas de la tabla y obtenemos las horas que ha realizado ese dia y si entran dentro de lo que su plan indica */
    for (let i = 0; i < table_data.length; ++i) { 
        numbers = table_data[i].hora.split(":")
        let horas = (numbers[0]*3600)
        let minutos = (numbers[1]*60)
        let segundos =  numbers[2]
        if(total_worked === false){
            total_worked = horas + minutos + parseInt(segundos)
        }else{
            let worked = horas + minutos + parseInt(segundos)
            total_worked = worked - total_worked
        }
      data.push({
        key: i,
        hora:table_data[i].hora,
        entrada:table_data[i].entrada ?<CheckCircleTwoTone twoToneColor="#52c41a" /> :<CloseCircleTwoTone twoToneColor="red" />,
        salida:table_data[i].salida?<CheckCircleTwoTone twoToneColor="#52c41a" /> :<CloseCircleTwoTone twoToneColor="red" />,
        asuencia:table_data[i].asuencia?<CheckCircleTwoTone twoToneColor="#52c41a" /> :<CloseCircleTwoTone twoToneColor="red" />,
        vacaciones:table_data[i].vacaciones?<CheckCircleTwoTone twoToneColor="#52c41a" /> :<CloseCircleTwoTone twoToneColor="red" />,
        auco:table_data[i].ausenciaconfirmada?<CheckCircleTwoTone twoToneColor="#52c41a" /> :<CloseCircleTwoTone twoToneColor="red" />,
        vaco:table_data[i].vacacionesconfirmadas?<CheckCircleTwoTone twoToneColor="#52c41a" /> :<CloseCircleTwoTone twoToneColor="red" />
      });
    }
    
    total_worked = total_worked / 3600
    
    
    return <Table columns={columns} dataSource={data} pagination={false} footer={false}
            footer={() => 
                table_data[0].plan.horas_dia > total_worked 
                ? <Text type="danger">Dia incumplido a falta de: {table_data[0].plan.horas_dia - total_worked } horas</Text>
                : <Text style={{color:"green"}}>Dia cumplido</Text>
            } />;
}
const columns = [
    {
        title: 'Day',
        dataIndex: 'title',
        key: 'title',
    },
];
class SeeRegistro extends React.Component {

    constructor(props){
        super(props)
         this.state = {
            dataSource:[]
        }
        console.log(props.match.params.user);
    }
    componentDidMount(){/* Obtenemos los registros del usuario */
        this.props.getRegistro({
            empresa_name:this.props.currentEmpresa,
            user:this.props.match.params.user
        })
    }
    shouldComponentUpdate(nextProps,nextState){
        /* Cuando el componente deje de estar estado de carga y los datos lleguen de forma correcta */
        if(nextProps.EmRe_loading !== this.props.EmRe_loading){
            if(nextProps.EmRe_data !== this.props.EmRe_data && nextProps.EmRe_data.selectAllRegistros !== undefined){
                let data_structure= []
                let dataSource = []
                let count=-1
                let actualDate=""
                /* Generamos la estructura de datos teniendo como indice la fecha de cada grupo de registros */
                nextProps.EmRe_data.selectAllRegistros.forEach(value => {
                    if (actualDate !== value.fecha) {
                        count++
                        actualDate = value.fecha
                        data_structure.push({title:value.fecha,description:[value]})
                    }else{
                        data_structure[count] = {...data_structure[count],description:[...data_structure[count].description, value]} 
                    }
                });
                /* Generamos la estructura de la tabla añadiendo las filas que se expanden */
                data_structure.forEach(value=>{ 
                    dataSource = [...dataSource, {key:value.title,title:value.title,description:expandedRowRender(value.description)}]
                })
                /* Forzamos el rerenderizado del componente */
                this.setState({...this.state,dataSource:dataSource})
                
                return true
            }
            if(nextProps.EmRe_error){
                return true
            }
            return true
        }
        if(nextState.dataSource !== this.state.dataSource){
            return true
        }
        return false
    }
    render(){
        return(
            /* Creamos una tabla que permita filas que se expandan */
            <Table
                className="components-table-demo-nested"
                columns={columns}
                expandable={{
                    expandedRowRender: record => record.description,
                    rowExpandable: record => record.description !== undefined ? 'Reload to get more info' :"",
                }}
                footer={false}
                dataSource={this.state.dataSource}>

            </Table>
        )
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(SeeRegistro)