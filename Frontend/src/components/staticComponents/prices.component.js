import React from "react"
import { Card, Col, Row } from 'antd';
import { CheckCircleTwoTone, StopTwoTone} from '@ant-design/icons';
import { Button } from 'antd';
import "./prices.style.css"
import { Link } from "react-router-dom";
class Prices extends React.Component{
    render(){
        return(
           
            <div style={{  padding: "30px"}}>
                <Row gutter={16}>
                    <Col span={8}>
                        <Card  title="Free Account" bordered={false}>
                            <table>
                                <tbody>
                                    <tr>
                                        <td className="prices-name"><span >Precio</span></td><td><span>Free</span></td>
                                    </tr>
                                    <tr>
                                        <td className="prices-name"><span >Empleados Maximos</span> </td><td><span>5</span></td>
                                    </tr>
                                    <tr>
                                        <td className="prices-name"><span >Precio por empleado</span></td><td><span>---</span></td>
                                    </tr>
                                    <tr>
                                        <td className="prices-name"><span >Planes(Turno)</span></td><td>3</td>
                                    </tr>
                                    <tr>
                                        <td className="prices-name"><span >Gestor Vacaciones</span></td><td> <StopTwoTone twoToneColor="red" /></td>
                                    </tr>
                                    <tr>
                                        <td className="prices-name"><span >Gestor Ausencias</span></td><td><StopTwoTone twoToneColor="red" /></td>
                                    </tr>
                                    <tr>
                                        <td className="prices-name"><span >Generar Resúmenes</span></td><td><CheckCircleTwoTone twoToneColor="#52c41a" /></td>
                                    </tr>
                                    <tr>
                                        <td className="prices-name"><span >Anticipación de Incidencias</span></td><td><StopTwoTone twoToneColor="red" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/><Link to="/register"><Button block>Registrar Empresa Gratis</Button></Link>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card  title="Pequeña Empresa" bordered={true}>
                        <table>
                                <tbody>
                                <tr>
                                    <td className="prices-name"><span >Precio</span></td><td><span>5€/mes</span></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Empleados Maximos</span> </td><td><span>50</span></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Precio por empleado</span></td><td><span>0.5€/empleado</span></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Planes(Turno)</span></td><td>5</td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Gestor Vacaciones</span></td><td><CheckCircleTwoTone twoToneColor="#52c41a" /></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Gestor Ausencias</span></td><td><CheckCircleTwoTone twoToneColor="#52c41a" /></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Generar Resúmenes</span></td><td><CheckCircleTwoTone twoToneColor="#52c41a" /></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Anticipación de Incidencias</span></td><td><StopTwoTone twoToneColor="red" /></td>
                                </tr>
                                </tbody>
                            </table>
                            <br/><Link to="/register"><Button type="primary" block>Empezar ahora</Button></Link>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card  title="Paquete Completo" bordered={false}>
                        <table>
                        <tbody>
                                <tr>
                                    <td className="prices-name"><span >Precio</span></td><td><span>10€/mes</span></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Empleados Maximos</span> </td><td><span>Ilimitados</span></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Precio por empleado</span></td><td><span>0.5€/empleado</span></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Planes(Turno)</span></td><td>Ilimitados</td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Gestor Vacaciones</span></td><td> <CheckCircleTwoTone twoToneColor="#52c41a"/></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Gestor Ausencias</span></td><td><CheckCircleTwoTone twoToneColor="#52c41a"/></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Generar Resúmenes</span></td><td><CheckCircleTwoTone twoToneColor="#52c41a" /></td>
                                </tr>
                                <tr>
                                    <td className="prices-name"><span >Anticipación de Incidencias</span></td><td><CheckCircleTwoTone twoToneColor="#52c41a"/></td>
                                </tr>
                                </tbody>
                            </table>
                            <br/><Link to="/register"><Button type="primary" block>Obtener Plan Completo</Button></Link>
                        </Card>
                    </Col>
                </Row>
            </div>

        )
    }

}

export default Prices
