import {notification } from 'antd';

/**
 * data= {
 *  type: (success,info,warning,error),
 *  title: String,
 *  description: String
 *  placement: (topRight, bottomRight, bottomLeft, topLeft),
 *  icon: Antd icon or functional component with an icon
 * }
 */

const openNotification = (data) => {
    notification[data.type]({
      message: data.title,
      description:data.description,
      placement:data.placement ? data.placement : "topRight",
      icon: data.icon ? data.icon : ""
    });
  };

export {
    openNotification
}