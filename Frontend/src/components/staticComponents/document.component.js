import React from 'react';
import ReactToPdf from "react-to-pdf"
import {Button } from 'antd';
export class TestPdf extends React.Component {
  render(){
    const ref = React.createRef();
      return(
        <div>
            <ReactToPdf targetRef={ref} filename="div-blue.pdf">
                {({toPdf}) => (
                    <Button onClick={toPdf}>Generate pdf</Button>
                )}
            </ReactToPdf>
            <div ref={ref}>{this.props.element}</div>
        </div>
      )
  }

}


