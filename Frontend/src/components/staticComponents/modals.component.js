import React from "react"
import { Modal, Button } from 'antd';
/**
 * Props = tittle, onOk, modalContent, with:{ok:Bool,no:Bool}
 */
class MakeModal extends React.Component {
  state = {
    loading: false,
    visible: false,
  };

  componentDidUpdate(prevProps){
      if(this.props.forceToShowModal===false && this.state.visible===true){
          console.log("test");
            this.setState({
                ...this.state, visible:false
            })
      }
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    this.setState({ loading: false, visible: false });

  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  render() {
    let { visible } =  this.state;
    return (
      <div>
        <Button type="primary" onClick={this.showModal}>
          {this.props.title}
        </Button>
        <Modal
          visible={visible}
          title={this.props.title}
          onOk={()=>this.handleOk()}
          onCancel={this.handleCancel}
          okButtonProps={{ disabled: this.props.disable.ok }}
          cancelButtonProps={{ disabled: this.props.disable.no  }}
        >
          {this.state.visible ?this.props.modalContent:<></>}
        </Modal>
      </div>
    );
  }
}

export default MakeModal