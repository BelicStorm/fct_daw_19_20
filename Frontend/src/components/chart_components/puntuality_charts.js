import React from 'react';
import {connect} from 'react-redux'
import { Card, Row, Col } from 'antd';
import {actions} from "../../duck/index"

import {PieChart,TailChart} from "./charts"


const mapStateToProps = (state) => {
  return {
      //auth state
      currentEmpresa:state.AUTH_REDUCER.currentEmpresa,
      //registros state
      EmRe_loading:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_loading,
      EmRe_error:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error,
      EmRe_error_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error_data,
      EmRe_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_data,
  }
}
const mapDispatchToProps = dispatch =>({ 
  getAllRegistros:(variables)=>dispatch(actions.employeeRegistrosActions.FETCH_REGISTRO_ACTIONS.FETCH_ALL_REGISTROS(variables)),
})
/* Recoje los datos obtenidos por el componente y los formatea para
obtener una media de puntualidad de todos los trabajadores y una sesgando los trabajadores por turnos */
function puntualidadTotal(data){
  let all_fechas_and_names = []
  let data_sorted_by_dates = {}
  let plans = []
  let sourceData = [/* Media de puntualidad total */
    { item: 'Puntuales', count: 0 },
    { item: 'Retrasos', count: 0 },
    { item: 'Nada Puntuales', count: 0 },
  ];
  let sourceData2 = []/* Media de puntualidad sesgada */
  data.map(value=>{/* Formamos el objeto que dividira nuestros datos por fechas y usuarios */
    if(!all_fechas_and_names.includes(value.fecha)){
      /* Si la fecha no existe en el array la ponemos y añadimos un nuevo indice al objeto */
      all_fechas_and_names.push(value.fecha)
      data_sorted_by_dates={...data_sorted_by_dates,[value.fecha]:{}}
    } 
    /* Añadimos al objeto de la fecha en concreto un array con el nombre del usuario como indice */
    data_sorted_by_dates[value.fecha]={...data_sorted_by_dates[value.fecha],[value.user_mail]:[]}                                       
  })
  data.map(value=>{/* Formateado ya el array añadimos a cada array de usuario los registros efectuados por el ese dia */
    let expected_hora = value.entrada === true ? value.plan.hora_entrada : value.plan.hora_salida
    data_sorted_by_dates[value.fecha][value.user_mail].push({
      date:value.fecha,
      entrada:value.entrada,
      hora:value.hora,
      expexted_hora:expected_hora,
      plan:value.plan.plan_name
    })
  })
  /* Teniendo los datos ya ordenados solo queda fromatear los dataSources de los graficos */
  for (const date in data_sorted_by_dates) {/* Recorremos las fechas */
    for (const user in data_sorted_by_dates[date]) {/* Dentro de cada fecha recorremos lo usuarios */
      let primera_entrada = false
      data_sorted_by_dates[date][user].map(registro=>{/* Dentro de los usuarios recorremos los registros */
        if(registro.entrada && primera_entrada===false){ /* Si el registro es la primera entrada el dia */
          let fichada = registro.hora.split(":")[0]
          let retrasoDe= (parseInt(registro.expexted_hora)-parseInt(fichada))*60
          let include = plans.includes(registro.plan_name);
          if(!include) plans.push(registro.plan);
          /* Dependiendo de los minutos que el empleado se haya retrasado añadiremos 
          o actualizaremos el dataSource pertinente */
             if(retrasoDe === 0 && retrasoDe > -30){
               sourceData2.push({plan: registro.plan,year:registro.date,value:100})
               sourceData[0].count = sourceData[0].count+1
              }
             if(retrasoDe >= -31 && retrasoDe >= -60){
              sourceData2.push({plan: registro.plan,year:registro.date,value:50})
               sourceData[1].count = sourceData[1].count+1
              }
             if(retrasoDe < -61){
              sourceData2.push({plan: registro.plan,year:registro.date,value:0})
               sourceData[2].count = sourceData[2].count+1
              }
              primera_entrada = true
        }
      })
    }
  }  
  
  
  return [sourceData,sourceData2]
}
class Puntuality extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      sourceData:false,
      sourceData2:false,
    }
  }
  /* Al montarse el componente obtenemos todos los registros de todas las tablas de registros de los usuarios de la empresa */
  componentDidMount(){
    this.props.getAllRegistros({empresa_name:this.props.currentEmpresa})
  }
  shouldComponentUpdate(nextProps,nextState){
    /* Al obtener los datos llamamos a la funcion que los formateara para que se puedan montar los graficos */
    if(nextProps.EmRe_loading !==this.props.EmRe_loading && nextProps.EmRe_data.selectPlanFromALlTablesOfAnEmpresa !==undefined){
      let data = nextProps.EmRe_data.selectPlanFromALlTablesOfAnEmpresa
      let sourcesData = puntualidadTotal(data)
      /* Al obtener los datos formateados forzamos un renderizado para que los componentes 
      de los graficos obtengan los datos actualizados */
      this.setState({...this.state, sourceData:sourcesData[0],sourceData2:sourcesData[1]})
      return true
    }
    if(nextState.sourceData !== this.state.sourceData){
      return true
    }
    
    return false
  }
  render() {
    
    return (
      <React.Fragment>
        <Card>
                <Row>
                    <Col span={10}>
                        <Card bordered={false} style={{height:"100%"}}>
                            {!this.state.sourceData ? "" : <PieChart sourceData={this.state.sourceData}></PieChart>}
                        </Card>
                    </Col>
                    <Col span={14}>
                        <Card bordered={false} style={{height:"100%"}}>
                            {!this.state.sourceData2 ? "" : <TailChart sourceData={this.state.sourceData2}></TailChart>}
                        </Card>
                    </Col>
                </Row>
            </Card>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Puntuality)