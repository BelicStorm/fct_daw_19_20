import { Chart, Tooltip, Axis, Legend, Coord, StackArea, StackLine, Pie } from 'viser-react';
import React from 'react';
const DataSet = require('@antv/data-set');

export function PieChart(props){
    const scale = [{
        dataKey: 'percent',
        min: 0,
        formatter: '.0%',
      }];
      const dv = new DataSet.View().source(props.sourceData);
      dv.transform({
        type: 'percent',
        field: 'count',
        dimension: 'item',
        as: 'percent'
      });
    const data = dv.rows;
    return(
        <Chart height={400} data={data} scale={scale}>
          <Tooltip showTitle={false} />
          <Axis />
          <Legend dataKey="item" />
          <Coord type="theta" radius={0.75} innerRadius={0.6} />
          <Pie position="percent" color="item" style={{ stroke: '#fff', lineWidth: 1 }}
            label={['percent', {
              formatter: (val, item) => {
                return item.point.item + ': ' + val;
              }
            }]}
          />
        </Chart>
    )
}

export function TailChart(props){
  console.log(props);
  
  const scale = [
 {
      dataKey: 'value',
      type: 'linear',
      tickInterval: 10
    }
  ];
  return(
    <Chart forceFit height={500} data={props.sourceData} scale={scale}  >
        <Tooltip
            crosshairs={{
                type: 'line'
            }}
            useHtml={false}
        />
        <Axis />
        <Legend />
        <StackArea
            position="year*value"
            color="plan"
        />
        <StackLine
            position="year*value"
            color="plan"
            size={2}
        />
    </Chart>
    )
}