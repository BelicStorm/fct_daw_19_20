import React from "react"
import SignAtWork from "../RegistrosComponents/SignAtWork/SignAtWork.component"
import UserPlanTable from "../PlansComponents/userPlan.component"
import { Card, Row, Col } from 'antd';

class EmployeeHome extends React.Component{
    render(){
        return(
            <React.Fragment>
                
            <Card>
                <Row>
                    <Col span={10}>
                        <Card>
                            <UserPlanTable></UserPlanTable>
                        </Card>
                    </Col>
                    <Col span={14}>
                        <Card style={{height:"100%"}}>
                            <SignAtWork></SignAtWork>
                        </Card>
                    </Col>
                </Row>
            </Card>
           
        </React.Fragment>
           
        )
    }

}

export default EmployeeHome

