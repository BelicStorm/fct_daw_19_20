import React from "react"
import {Table,Spin} from 'antd';
import {connect} from 'react-redux'
import {actions} from "../../duck/index"
import MakeModal from "../staticComponents/modals.component"
import {openNotification} from "../staticComponents/notifications.utils"
import {PlansForm} from "./plansForm.component.js"

const mapStateToProps = (state) => {
    return {
        empresa:state.AUTH_REDUCER.currentEmpresa,
        loading:state.MANAGE_PLANS_REDUCER.plans_loading,
        error:state.MANAGE_PLANS_REDUCER.plans_error,
        error_data:state.MANAGE_PLANS_REDUCER.plans_error_data,
        data:state.MANAGE_PLANS_REDUCER.plans_data,
    }
}
const mapDispatchToProps = dispatch =>({ 
    getAllPlans:()=> dispatch((actions.managePlansActions.PLANS_FETCH_ACTIONS.SELECT_ALL_PLANS_FETCH())),
    insertPlan:(plan)=>dispatch((actions.managePlansActions.INSERT_PLAN_ACTIONS.INSERT_PLAN(plan))),
})
const columns = [/* Creamos las columnas de nuestra tabla */
    {
        title: 'Name',
        dataIndex: 'plan_name',
        key: 'plan_name',
    },
    {
        title: 'Horas Totales',
        dataIndex: 'horas_dia',
        key: 'horas_dia',
    },
    {
        title: 'Vacaciones',
        dataIndex: 'vacaciones',
        key: 'vacaciones',
    },
    {
        title: 'Entrada',
        dataIndex: 'hora_entrada',
        key: 'hora_entrada',
    },
    {
        title: 'Salida',
        dataIndex: 'hora_salida',
        key: 'hora_salida',
    },
  ];
class ManagePlans extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            dataSource:[],
            plans:[]
        }
        this.createPlan = this.createPlan.bind(this)
        this.ModalController = React.createRef();
    }
    createPlan(e){
        /* El nombre del plan no puede ser Base ni estar vacio */
       if(e.plan_name ==="Base" || (e.plan_name.trim()).length === 0){
        openNotification({type:"error",title:"Plan Name Error",description:`${e.plan_name} can't be use as plan name`})
       }else{
           /* Obtenemos del fomulario la hora de entrada y la de salida */
            let hora_entrada = e.schedule[0]._d.getHours()
            let hora_salida = e.schedule[1]._d.getHours()
            let plan = {/* Formatamos los datos para insertar el nuevo plan */
                plan_name:e.plan_name,
                horas_dia:(hora_salida-hora_entrada),
                vacaciones:parseInt(e.vacaciones),
                hora_entrada:`${hora_entrada}`,
                hora_salida:`${hora_salida}`
            }
            /* Cambiamos el estado para renderizar de nuevo la tabla */
            this.setState({...this.state,
                dataSource:[...this.state.dataSource,{
                    key:e.plan_name,plan_name:e.plan_name,horas_dia:(hora_salida-hora_entrada),
                    vacaciones:e.vacaciones,hora_entrada:hora_entrada,hora_salida:hora_salida
                }],
            plans:[...this.state.plans, plan]})
            /* Insertamos el nuevo plan */
            this.props.insertPlan({...plan,empresa: this.props.empresa})
            /* Cerramos el modal */
            this.ModalController.current.handleCancel();
       }
        
    }
    /* Cuando el componente se monta obtenemos todos los planes que hay creados hasta el momento */
    componentDidMount(){
        this.props.getAllPlans()
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.loading === true) { /* Siempre que se inicie una peticion en el servidor */
            return true
        }
        /* Cuando obtengamos los datos formateamos el datasource que nutrira nuestra tabla */
        if (nextProps.data.getAllPlans!==undefined && this.props.loading === true && nextProps.loading===false) {
            let dataSource = []
            nextProps.data.getAllPlans.map((plans,index)=>{
                return dataSource = [...dataSource,{
                    key:plans.plan_name,plan_name:plans.plan_name,horas_dia:plans.horas_dia,
                    vacaciones:plans.vacaciones,hora_entrada:plans.hora_entrada,hora_salida:plans.hora_salida
                }]
            })
            /* Al cambiar el estado obligamos a rerenderizar de nuevo el componente */
            this.setState({...this.state,dataSource:dataSource, plans:nextProps.data.getAllPlans})
            return true
        }
        /* Si el plan se ha creado de forma correcta mostraremos una notificacion */
        if(nextProps.data.createPlan!==undefined  && nextProps.loading===false){
            openNotification({type:"success",title:"Plan Created Successfully", description:nextProps.data.message})
            return true   
        }
        /* En caso contrario, si ha habido un error, mostraremos una notificacion con el error y recargaremos la pagina */
        if (nextProps.error===true && this.props.loading ===true) {
            console.log(nextProps);
            openNotification({type:"error",title:"Error", description:nextProps.error_data.message})
            setTimeout(() => {
                window.location.reload(); 
            }, 1200);
            return true
        }
        /* Cuando los datos de la tabla cambien reRenderizamos el componente */
        if (nextState.dataSource!==this.state.dataSource) {
            return true
        }
        return false

    }
    render(){
        return(
            <React.Fragment>
                <h2>Manage Plans</h2>
                <Spin spinning={this.props.loading}>
                    {<Table columns={columns} dataSource={this.state.dataSource}
                        pagination={{showSizeChanger:true,position:["bottomCenter"]}} />}

                    <MakeModal disable={{ok:true,no:false}} title="Create New Plan" 
                           modalContent={<PlansForm onFinish={this.createPlan} ></PlansForm>}
                           destroyOnClose={true} ref={this.ModalController} forceRender={true}
                    ></MakeModal>
                </Spin>
                
            </React.Fragment>
        )
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(ManagePlans)