import React from "react"
import { Form, TimePicker, Input, Button, Typography,InputNumber} from 'antd';
const { Title } = Typography;
const { RangePicker } = TimePicker;

export const PlansForm = (props) =>{
    return(
        <React.Fragment>
            <Form name="complex-form" onFinish={props.onFinish} labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
                <Title  style={{ marginLeft:"30%", }}>
                    Create a new plan 
                </Title>
                <Form.Item label="Plan Name">
                    <Form.Item
                        name="plan_name"
                        noStyle
                        rules={[{ required: true, message: 'Plan name is required' }]}
                    >
                         <Input style={{ width: 160 }} placeholder="Plan Name" />
                    </Form.Item>
                </Form.Item>
                <Form.Item label="Schedule">
                    <Form.Item
                        name="schedule"
                        noStyle
                        rules={[{ required: true, message: 'Schedule is required' }]}
                    >
                        <RangePicker format={"HH"}/>
                    </Form.Item>
                </Form.Item>
                <Form.Item label="Vacation Days">
                    <Form.Item
                        name="vacaciones"
                        noStyle
                        rules={[{ required: true, message: 'Vacation days are required' }]}
                    >
                         <InputNumber style={{ width: 160 }} placeholder="Vacation Days" />
                    </Form.Item>
                </Form.Item>
                <Form.Item label=" " colon={false}>
                    <Button type="primary" htmlType="submit">
                        Create
                    </Button>
                </Form.Item>
            </Form>
        </React.Fragment>
    )
    /* horas_dia:$horas_dia,
    vacaciones:$vacaciones, */


}