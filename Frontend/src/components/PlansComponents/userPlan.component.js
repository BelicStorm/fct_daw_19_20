import React from "react"
import {connect} from 'react-redux'
import { Spin } from 'antd';
import {actions} from "../../duck/index"


const mapStateToProps = (state) => {
    return {
        empresa:state.AUTH_REDUCER.currentEmpresa,
        user_plan:state.AUTH_REDUCER.user_plan,
        loading:state.MANAGE_PLANS_REDUCER.plans_loading,
        error:state.MANAGE_PLANS_REDUCER.plans_error,
        error_data:state.MANAGE_PLANS_REDUCER.plans_error_data,
        data:state.MANAGE_PLANS_REDUCER.plans_data,
    }
}
const mapDispatchToProps = dispatch =>({ 
    onLoad:(variables)=>{dispatch(actions.managePlansActions.PLANS_FETCH_ACTIONS.SELECT_PLAN_BY_ID(variables))}
})
class UserPlanTable extends React.Component{
    componentDidMount(){/* Cuando el componente carga obtenemos de los props a que empresa  */
        this.props.onLoad({empresa:this.props.empresa,plan_id:parseInt(this.props.user_plan)})
    }
    shouldComponentUpdate(nextProps,nextState){
        if(nextProps.loading !== this.props.loading){
            return true
        }
        return false
    }
    render(){
        return (
     <div>
            <Spin  spinning={this.props.loading} delay={500}>
                {this.props.data.getPlanById !==undefined 
                    ? <ul style={{padding: 0, listStyle: "none",color: "#b0dbd9",margin: "5px 0 5px 10px", textShadow: "0 1px 0 white" }}>
                            <li>{this.props.data.getPlanById.plan_name}</li>
                            <li></li>
                            <li>Hora de entrada:  {this.props.data.getPlanById.hora_entrada}h</li>
                            <li>Hora de salida:   {this.props.data.getPlanById.hora_salida}h</li>
                            <li>Horas a realizar: {this.props.data.getPlanById.horas_dia}h</li>
                        </ul>
                    : <h2>Loading...</h2>
                
                }
            </Spin>
            
     </div>
    );
    }
    
};
export default connect(mapStateToProps, mapDispatchToProps) (UserPlanTable)
