import React from "react"
import {connect} from 'react-redux'
import {RegisterNewUserForm} from "../auth/authForms.component"
import {actions} from "../../duck/index"
import {Table,Spin,Tag,Tooltip, Button } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import MakeModal from "../staticComponents/modals.component"
import {openNotification} from "../staticComponents/notifications.utils"
import { Link } from "react-router-dom";
import {TestPdf} from "../staticComponents/document.component"

const mapStateToProps = (state) => {
    return {
        //auth state
        auth_loading:state.AUTH_REDUCER.auth_loading,
        auth_error:state.AUTH_REDUCER.auth_error,
        auth_error_data:state.AUTH_REDUCER.auth_error_data,
        auth_data:state.AUTH_REDUCER.auth_data,
        currentEmpresa:state.AUTH_REDUCER.currentEmpresa,
        //manage employees state
        loading:state.MANAGE_EMPLOYEES_REDUCER.MaEm_loading,
        error:state.MANAGE_EMPLOYEES_REDUCER.MaEm_error,
        error_data:state.MANAGE_EMPLOYEES_REDUCER.MaEm_error_data,
        data:state.MANAGE_EMPLOYEES_REDUCER.MaEm_data,
        //plans state
        plans_loading:state.MANAGE_PLANS_REDUCER.plans_loading,
        plans_error:state.MANAGE_PLANS_REDUCER.plans_error,
        plans_error_data:state.MANAGE_PLANS_REDUCER.plans_error_data,
        plans_data:state.MANAGE_PLANS_REDUCER.plans_data,
        //registros state
        EmRe_loading:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_loading,
        EmRe_error:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error,
        EmRe_error_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_error_data,
        EmRe_data:state.EMPLOYEE_REGISTROS_REDUCER.EmRe_data,
    }
}
const mapDispatchToProps = dispatch =>({ 
    getAllUsers:()=> dispatch((actions.manageEmployeesActions.FETCH_USERS_ACTIONS.FETCH_USERS())),
    getAllPlans:()=> dispatch((actions.managePlansActions.PLANS_FETCH_ACTIONS.SELECT_ALL_PLANS_FETCH())),
    insertUser:(variables)=>dispatch(actions.authActions.REGISTER_NEW_EMPLOYEE_ACTIONS.REGISTER_NEW_EMPLOYEE(variables)),
    createRegistroTable:(variables)=>dispatch(actions.employeeRegistrosActions.CREATE_REGISTRO_TABLE_ACTIONS.CREATE_REGISTRO_TABLE(variables)),
})
const columns = [/* Indicamos las columnas que va a tener la tabla  */
    {
        title: 'User Email',
        dataIndex: 'email',
        key: 'email',
    },
    {
        title: 'User Name',
        dataIndex: 'nombre',
        key: 'nombre',
    },
    {
        title: 'User Position',
        dataIndex: 'user_type',
        key: 'user_type',
    },
    {
        title: 'Manage Registros',
        dataIndex: 'manage_registros',
        key: 'manage_registros',
    },
    {
        title: 'Plan',
        dataIndex: 'plan',
        key: 'plan',
    }
];
function userType(user_type) {/* Retornamos un Ant Ui Component con la categoria el empleado */
    switch (user_type) {
        case "Jefe":
            return <Tag color="magenta">{user_type}</Tag>  
        case "Admin":
            return <Tag color="green">{user_type}</Tag>
        default:
            return <Tag color="blue">{user_type}</Tag>    
    }        
}

class ManageEmployees extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            dataSource:[],
            users:[],
            plans:[]
        }
        this.handleCloseModal = React.createRef(); /* Referencia al metodo de cirerre del componente modal */
        this.registerNewEmployee = this.registerNewEmployee.bind(this)
    }
    componentDidMount(){ /* Cunado el componente cargue obtenemos todos los usuarios y todos los planes */
        this.props.getAllUsers()
        this.props.getAllPlans()
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.loading === true) { /* Cuando el estado del store de loading cambie, recargamos el componente */
            return true
        }
        if (this.props.loading === true && nextProps.loading===false) {
            let dataSource = []
            nextProps.data.SelectAllUsers.map((users,index)=>{/* De los datos obtenidos, los formateamos para crear la estructura de la tabla */
                return dataSource = [...dataSource,{
                    key:users.email,email:users.email,nombre:users.nombre,
                    user_type:userType(users.user_type),plan:users.plan.plan_name,
                    description:`Plan Name:${users.plan.plan_name} - Dias de vacaciones:${users.plan.vacaciones} -  
                    Horas Totales:${users.plan.horas_dia} - Hora de entrada:${users.plan.hora_entrada} - 
                    Hora de salida:${users.plan.hora_salida}`,
                    manage_registros:users.user_type!=="Jefe"
                                    ?<Link to={`/manageRegistros/${users.email}`}><Button>See Registros</Button></Link>
                                    :""
                   
                }]
            })
            this.setState({...this.state,dataSource:dataSource, users:nextProps.data.SelectAllUsers,
                    plans:[...this.state.plans,nextProps.plans_data.getAllPlans]})
            return true
        }
        if (nextState.dataSource!==this.state.dataSource) {/* Cuando el datasource cambie forzamos un render para pintar la tabla con los nuevos datos */
            return true
        }
        if(this.props.auth_loading === true && nextProps.auth_loading===false){
            if(nextProps.auth_error){ /* En caso de haber un error mostramos una notificacion */
                openNotification({type:"error",title:"Error", description:"error"})
                return false
            }
            this.handleCloseModal.current.handleCancel();/* Creamos una referencia que apunte al metodo de cancelado del componente modal */
            /* Abrimos una notificacion que avise que el usuario se ha creado correctamente */
            openNotification({type:"success",title:"User Created Successfully", description:nextProps.auth_data.message})
            /* Creamos la tabla de registros del usuario que acabamos de crear */
            this.props.createRegistroTable({
                empresa_name:this.props.currentEmpresa,
                user:this.state.users[this.state.users.length-1].email
            })
            return true
        }
        return false

    }
    registerNewEmployee(e){
        /* Obtenemos el id del plan que seleccionamos en el formulario */
        let plan_name = this.state.plans[0].filter(plan=>plan.id === e.plan_id)[0].plan_name
        this.setState({...this.state,
                       dataSource:[...this.state.dataSource,{ /* Seteamos el datasource para pintar la tabla */
                        key:e.email,email:e.email,nombre:e.nombre,plan:plan_name,
                        user_type:e.user_type ==="Jefe" ? <Tag color="magenta">{e.user_type}</Tag> : <Tag color="blue">{e.user_type}</Tag>
                       }],
                       users:[...this.state.users,{...e,plan_id:parseInt(e.plan_id),id_empresa:this.state.users[0].id_empresa}]})
        this.props.insertUser({...e,plan_id:parseInt(e.plan_id),id_empresa:this.state.users[0].id_empresa})
        
    }
    render(){        
        return(
           <React.Fragment>
               {/* Mientras esta el prop de login bloqueamos la tabla y los botones */}
                <Spin spinning={this.props.loading}>
                    <Spin spinning={this.props.plans_loading}>
                        <Tooltip title="Click the + button to see the Employee Plan in more detail. If you create a new user, you will see the button when you reload the page ">
                            <h2>Manage Employees
                                <QuestionCircleOutlined style={{ display: 'inline-block', width: 'calc(10% - 8px)', margin: '0 8px' }}></QuestionCircleOutlined>
                            </h2>
                            
                        </Tooltip>
                        <TestPdf element={ /* Exportamos como pdf la tabla generada */
                            <Table columns={columns} dataSource={this.state.dataSource}
                                pagination={{showSizeChanger:true,position:["bottomCenter"]}}
                                expandable={{/* Indicamos que cada fila de la tabla se expande con la informacion del plan que tiene el
                                    empleado*/
                                    expandedRowRender: record => <p style={{ margin: 0 }}>{record.description}</p>,
                                    rowExpandable: record => record.description !== undefined ? 'Reload to get more info' :"",
                                }}
                            />
                        }></TestPdf>
                        {/* Generamos el modal que contiene el formulario de creacion de nuevos usuarios.
                        Le pasamos los planes, el puntero de metodo del registro de empleados y la referencia para cerrar el modal */}
                        <MakeModal disable={{ok:true,no:false}} title="Create New User" 
                                modalContent={<RegisterNewUserForm plans={this.state.plans} onFinish={this.registerNewEmployee}/>}
                                destroyOnClose={true} ref={this.handleCloseModal} forceRender={true}
                        ></MakeModal>
                        
                    </Spin>
                </Spin>

                
           </React.Fragment>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageEmployees)