import React from 'react';
import { useSelector } from "react-redux";
import './header.style.css'
import Logo from "./LogoOk.png"
import { Layout, Menu} from 'antd';
import {DesktopOutlined, PieChartOutlined,FileOutlined,UserOutlined} from '@ant-design/icons';
import { Link } from 'react-router-dom';
const { Sider } = Layout;

/* MakeSliderMenu se encarga de crear el menu de los usuarios logeados. Dependiendo de el tipo de usuario mostraremos un menu diferente*/
const MakeSliderMenu = (props)=>{
    const userType = useSelector(state => state.AUTH_REDUCER.user_type)
    /* Menu comun para todos los usuarios */
    let menu=[
        {path:"/",name:"Home",img:<DesktopOutlined/>},
    ]
    /* Completamos el menu comun con los diferentes paths */
    switch (userType) {
        case "Jefe":
            menu = [...menu,{path:"/manage_employees",name:"Manage Personal", img:<PieChartOutlined/>},
                            {path:"/manage_plans",name:"Manage Plans", img:<FileOutlined/>},]
            break;
        case "Admin":
            menu = [...menu,{path:"/manage_employees",name:"Manage Personal", img:<PieChartOutlined/>}]
            break;
        default:
            break;
    }
    return(
        <Sider collapsible collapsed={props.collapsed} onCollapse={props.onCollapse} breakpoint="lg" collapsedWidth="0">
            <div className="logo">
                <Link to="/"><img src={Logo} alt="Logo"></img></Link>
                <span style={{color:"white", marginLeft:"50%",marginRight:"50%"}}>{props.info.empresa} </span>    
            </div> 
            <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                <Menu.Item>
                    <UserOutlined/>{props.info.user}
                </Menu.Item>{/* Creamos el menu a partir del array customizado anteriormente */}
                    {menu.map(menuItem=>{
                        return [
                            <Menu.Item>
                                {menuItem.img}
                                <Link to={menuItem.path}><span>{menuItem.name}</span></Link>
                            </Menu.Item>
                        ]
                    })}
            </Menu>
        </Sider>
    )
}

class Header extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            collapsed:true
        }
        console.log(this.props.userInfo.user);
        
        this.toggleCollapsed = this.toggleCollapsed.bind(this)
    }
    toggleCollapsed = () => {
        this.setState({
          collapsed: !this.state.collapsed,
        });
      };
    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.collapsed !== this.state.collapsed) {
            return true
        }
        return false
    }
    render(){
        return(
          <React.Fragment>
            {
            /* Dependiendo de si hay usuario o no logeado cargaremos un menu u otro */
            this.props.userInfo.user===null
                ? /* Si no hay usuario logeado cargaremosun menu horizontal en el que solo tendremos el boton de login */
                    <React.Fragment>
                            <Menu theme="dark" mode="horizontal" >
                                <Menu.Item ><Link to="/"><img src={Logo} alt="Logo" style={{width: "20%"}}></img></Link></Menu.Item>
                                <Menu.Item key="1" className="auth_Button" style={{position: "fixed",right: 0,zIndex: 1}}>
                                        <Link to="/login">Login</Link>
                                </Menu.Item>
            
                            </Menu>
                    </React.Fragment>
                : /*  Si el usuario se encuentra logeado entonces cargaremos el menu vertical y pasaremos el puntero del metodo del componente Layout para 
                      deslogear al usuario
                  */
                    <React.Fragment>
                        {/* Componente que crea el menu vertical */}
                        <MakeSliderMenu collapsed={this.state.collapsed} onCollapse={this.toggleCollapsed}
                                        info={{user:this.props.userInfo.user,empresa:this.props.userInfo.empresa,user_type:this.props.userInfo.user_type}}/>
                        {/* Boton de deslogeo */}
                        <Menu theme="dark" mode="horizontal" style={{position: "fixed",right: 0,zIndex: 1}}>
                            <Menu.Item key="1" className="auth_Button" onClick={this.props.userInfo.logout}>LogOut</Menu.Item>
                        </Menu>
                    </React.Fragment>
            }

          </React.Fragment>
        )
    }
}

export default Header




