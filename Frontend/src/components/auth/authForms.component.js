import React from "react"
import { QuestionCircleOutlined } from '@ant-design/icons';
import { Form, Input, Tooltip ,Button, Typography, Select} from 'antd';
import Logo from "/home/cristian/Escritorio/cosas/tfm/fct_daw_19_20/Frontend/src/components/header/LogoOk.png"
import MakeModal from "../staticComponents/modals.component"
const { Title } = Typography;

const LoginForm = (props) => {
  return (
    <React.Fragment>
      <Form name="complex-form" onFinish={props.onFinish} labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
      <Title  style={{ marginLeft:"20%", }}>
        Login 
        <img src={Logo} alt="Logo" style={{ display: 'inline-block', width: 'calc(10% - 8px)', margin: '0 8px' }}></img>
      </Title>
      <Form.Item label="Company Name">
        <Form.Item  name="empresa" noStyle rules={[{ required: true, message: 'Company name is required' }]}>
          <Input style={{ width: 160 }} placeholder="Company Name" />
        </Form.Item>
        <Tooltip title="Enter the name of the company you work for ">
            <QuestionCircleOutlined style={{ width: 160 }}/>
        </Tooltip>
      </Form.Item>
      <Form.Item label="Email And Password" style={{ marginBottom: 0 }}>
        <Form.Item
          name="email"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(20% - 8px)' }}
        >
          <Input placeholder="Email" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(20% - 8px)', margin: '0 8px' }}
        >
          <Input placeholder="Password" />
        </Form.Item>
      </Form.Item>
      <Form.Item label=" " colon={false}>
        <Button type="primary" htmlType="submit">
          Login
        </Button>
      </Form.Item>
      
    </Form>
    </React.Fragment>
  );
};
const RegisterCompanyForm = ()=>{
  return(
    <React.Fragment>
      <Title  style={{ marginLeft:"20%", }}>
        Make your time count
        <img src={Logo} alt="Logo" style={{ display: 'inline-block', width: 'calc(10% - 8px)', margin: '0 8px' }}></img>
      </Title>
      <Form.Item label="Company Name and Account type" style={{ marginBottom: 0 }}>
        <Form.Item
          name="empresa_name"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(20% - 8px)' }}
        >
          <Input placeholder="Company Name" />
        </Form.Item>
        <Form.Item 
          name="account_type"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(20% - 8px)', margin: '0 8px' }}>
            <Select style={{ width: 120 }}>
              <Select.Option value="free">Free account</Select.Option>
              <Select.Option value="little">Little Company</Select.Option>
              <Select.Option value="full">Full Plan</Select.Option>
            </Select>
        </Form.Item>
      </Form.Item>
    </React.Fragment>
  )
}
const RegisterNewUserForm=(props)=>{
  return(
    <React.Fragment>
      <Form name="complex-form" onFinish={props.onFinish} labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
        {
          props.complete ?
          <RegisterCompanyForm></RegisterCompanyForm>
          :<></>
        }
      <Title  style={{ marginLeft:"20%", }}>
        Ingres the new User
        <img src={Logo} alt="Logo" style={{ display: 'inline-block', width: 'calc(10% - 8px)' }}></img>
      </Title>
      <Form.Item label="Email and Name" style={{ marginBottom: 0 }}>
        <Form.Item
          name="email"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(30% - 8px)' }}
        >
          <Input placeholder="User Email" />
        </Form.Item>
        <Form.Item 
          name="nombre"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(30% - 8px)' }}>
             <Input placeholder="User" />
        </Form.Item>
       
      </Form.Item>
      <Form.Item label="Password" style={{ marginBottom: 0 }}>
        <Form.Item  name="password"
          rules={[{ required: true }]}
          style={{ display: 'inline-block', width: 'calc(40% - 8px)' }}
        >
          <Input placeholder="Password" />
        </Form.Item>
      </Form.Item>
      <Form.Item label="Type" style={{ marginBottom: 0 }}>
        <Form.Item
          name="user_type"
          rules={[{ required: true }]}
        >
          <Select style={{ width: 120 }}>
            {
              props.complete 
              ? <Select.Option value="Jefe">Jefe</Select.Option>
              : <React.Fragment>
                  <Select.Option value="Admin">Admin</Select.Option>
                  <Select.Option value="Empleado">Empleado</Select.Option>
                </React.Fragment>
            }
              
          </Select>
        </Form.Item>
      </Form.Item>
      {
        props.plans!==undefined ?
            <Form.Item label="Plan" style={{ marginBottom: 0 }}>
              <Form.Item
                name="plan_id"
                rules={[{ required: true }]}
              >
                <Select key="select" style={{ width: 220 }}>       
                        {props.plans[0].map(plan=>{
                          if(plan.plan_name !=="Base"){
                          return (
                                <Select.Option key={plan.plan_name} value={plan.id}> {plan.plan_name} 
                                    <MakeModal disable={{ok:false,no:false}} title={plan.plan_name} 
                                        modalContent={
                                          <div>
                                             <span style={{color:"white"}}>Hora de Entrada: </span><span>{plan.hora_entrada}</span><br></br>
                                             <span style={{color:"white"}}>Hora de Salida: </span><span>{plan.hora_salida}</span><br></br>
                                             <span style={{color:"white"}}>Horas al dia: </span><span>{plan.horas_dia}</span><br></br>
                                             <span style={{color:"white"}}>Vacaciones: </span><span>{plan.vacaciones}</span><br></br>
                                          </div>
                                        }
                                        destroyOnClose={true} forceRender={true}
                                      ></MakeModal>

                                </Select.Option>
                              )
                          }
                          return ""
                        })}
                </Select>
                
              </Form.Item>
          </Form.Item>
      :<div></div>
      }
      <Form.Item label=" " colon={false}>
        <Button type="primary" htmlType="submit">
          Register
        </Button>
      </Form.Item>
     
    </Form>
    </React.Fragment>
  )
}

export {LoginForm,RegisterNewUserForm}


