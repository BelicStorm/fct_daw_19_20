import React from "react"
import {connect} from 'react-redux'
import {RegisterNewUserForm} from "./authForms.component"
import {actions} from "../../duck/index"
import { message } from 'antd';

const mapStateToProps = (state) => {
    return {
        plans_loading:state.MANAGE_PLANS_REDUCER.plans_loading,
        plans_error:state.MANAGE_PLANS_REDUCER.plans_error,
        plans_error_data:state.MANAGE_PLANS_REDUCER.plans_error_data,
        plans_data:state.MANAGE_PLANS_REDUCER.plans_data,

        loading:state.AUTH_REDUCER.auth_loading,
        error:state.AUTH_REDUCER.auth_error,
        error_data:state.AUTH_REDUCER.auth_error_data,
        data:state.AUTH_REDUCER.auth_data,
        next_step:state.AUTH_REDUCER.auth_next_step
    }
}
const mapDispatchToProps = dispatch =>({ 
    CreateCompany:(variables)=>dispatch(actions.authActions.REGISTER_COMPANY_ACTIONS.REGISTERCOMPANY(variables)),
    CreateTable:(variables)=>dispatch(actions.authActions.REGISTER_COMPANY_ACTIONS.CREATE_USER_TABLE(variables)),
    CreatePlanTable:(variables)=>dispatch(actions.authActions.REGISTER_COMPANY_ACTIONS.CREATE_PlAN_TABLE(variables)),
    insertUser:(variables)=>dispatch(actions.authActions.REGISTER_NEW_EMPLOYEE_ACTIONS.REGISTER_NEW_EMPLOYEE(variables)),
    insertPlan:(variables)=>dispatch(actions.managePlansActions.INSERT_PLAN_ACTIONS.INSERT_PLAN(variables)),
})

class Register extends React.Component {
    constructor(props){
        super(props)
        this.onClickRegisterCompany = this.onClickRegisterCompany.bind(this)
    }
    /* Al clickar recogemos los datos del formulario y disparamos la accion de insertar la empresa */
    onClickRegisterCompany(e){
        let empresa = {
            empresa_name:e.empresa_name,
            account_type:e.account_type
        }
        this.setState({
            user:{
                email: e.email,
                empresa_name: e.empresa_name,
                nombre: e.nombre,
                password: e.password,
                user_type: e.user_type
            } 
        })
        this.props.CreateCompany(empresa)
    }
    shouldComponentUpdate(nextProps, nextState) {
        /* Obtenemos del proximo estado del reducer que paso tiene que dar el registro */
        if (nextProps.next_step !== this.props.next_step && nextProps.next_step!==null) {
            switch (nextProps.next_step) {
                case "createPlanTable": /* Creamos la tabla de planes */
                    this.setState({...this.state, user:{...this.state.user, id_empresa:parseInt(nextProps.data.empresa.id)}})
                    this.props.CreatePlanTable({empresa:this.state.user.empresa_name})
                    break;
                case "createTable":/* Creamos la tabla de usuarios de la empresa */
                    this.props.CreateTable({created_empresa:this.state.user.empresa_name})
                    break;
                case "insertUser":/* Insertamos el plan base de la empresa */
                    if(!nextProps.error){
                        this.props.insertPlan({plan_name:"Base", empresa:this.state.user.empresa_name})
                    }
                    return false
                default:
                    return true
            }
            
            return true
        }
        if(nextProps.plans_loading===false && this.props.plans_loading===true && nextProps.plans_data.createPlan!==undefined){  
            this.props.insertUser(this.state.user) /* insertamos el usuario jefe */
            return true
        }
        return false
    }
    /* Cuando se registre el usuario redirigimos a la pantalla de login */
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.loading !== this.props.loading) {
            if(this.props.data.insertUser!==undefined){  
                message.success('Registered successfuly');
                window.location.href = "/login"
            }
        }
    }
    render(){
        return(
            <RegisterNewUserForm complete={true} onFinish={this.onClickRegisterCompany}></RegisterNewUserForm>
        )
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(Register)