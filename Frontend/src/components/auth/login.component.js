import React from "react"
import {connect} from 'react-redux'
import {actions} from "../../duck/index"
import {LoginForm} from "./authForms.component"
import { Spin } from 'antd';
import {openNotification} from "../staticComponents/notifications.utils"
 
const mapStateToProps = (state) => {
    return {
        loading:state.AUTH_REDUCER.auth_loading,
        error:state.AUTH_REDUCER.auth_error,
        error_data:state.AUTH_REDUCER.auth_error_data,
        data:state.AUTH_REDUCER.auth_data
    }
}
const mapDispatchToProps = dispatch =>({ /* Creamos una funcion que contenga todas las acciones 
    necesarias que modifiquen el store a nuestro placer. En este caso llamaremos a las acciones del pato
    asociado a esta funcionalidad, el cual se encargara de efectuar toda la logica de la peticion al servidor y de lanzar
    el action que modificara el reducer */
    login:(variables)=>dispatch(actions.authActions.LOGIN_ACTIONS.LOGIN(variables))
})

class Login extends React.Component{
    constructor(props){
        super(props)
        this.login = this.login.bind(this)
    }
    /* Cargamos la accion de login */
    login(e){
        this.props.login(e)
    }
    componentDidUpdate(prevProps, prevState) {
        /* Si cuando se actualice el estado de loading, almacenado en el reducer de auth, y
        no ha habido ningun error redirigimos a home */
        if (prevProps.loading !== this.props.loading) {
          if(this.props.error_data===null && !this.props.loading){
              window.location.href = "/"
          }else{ /* En caso contrario mostramos una notificacion con el error acontecido */
            openNotification({type:"error",title:"Login Error",description:"Some of the data are wrong"})
          }
        }
    }
    render(){
        return(
            /* Si el estado de la aplicacion es cargado, mostramos un spnier y bloqueamos el formulario de login */
            <Spin spinning={this.props.loading} delay={500}>
                <LoginForm onFinish={this.login}></LoginForm>
            </Spin>
            
        )
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(Login)