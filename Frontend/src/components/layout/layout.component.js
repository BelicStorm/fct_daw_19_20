import React from "react"
import Header from "../header/unlogedHeader.component"
import { Layout } from 'antd';
import {connect} from 'react-redux'
import {actions} from "../../duck/index"

const mapStateToProps = (state) => {
    return {
        currentUser:state.AUTH_REDUCER.currentUser,
        currentEmpresa:state.AUTH_REDUCER.currentEmpresa,
        currentUserType:state.AUTH_REDUCER.user_type
    }
}
const mapDispatchToProps = dispatch =>({ 
    logout:()=>dispatch(actions.authActions.LOGOUT_ACTIONS.LOGOUT())
})

//AppLayout se encarga de contener los diferentes tipos de cabeceras y los componentes que carga el router
class AppLayout extends React.Component{
    constructor(props){
        super(props)
        console.log(props);
        this.makeLogout= this.makeLogout.bind(this)
    }
    makeLogout(){
        this.props.logout() //Limpia el estado de la sesion del usuario
        window.location.href = "/"
    }
    render(){
        return(
            <Layout>
                {/* El componente header obtiene mediante props el usuario actual, su tipo y la empresa a la que pertenece.
                    Tambien obtiene el puntero al metodo que efectua el Logout
                */}
                <Header userInfo={{user:this.props.currentUser,empresa:this.props.currentEmpresa,
                                  logout:this.makeLogout,user_type:this.props.currentUserType}}/>
                    <Layout.Content className="site-layout" style={{ padding: '0 50px', marginTop: 30, minHeight:700}}>
                        <div  style={{ padding: 24, minHeight: "100%" }}>
                            {this.props.children /* Componente cargado por el router */ } 
                        </div>
                    </Layout.Content>
            </Layout>
               

        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppLayout)

