import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
///Componentes Padre
import NoLogedHome from "../components/home/noLogedHome.component"
import AdminHome from "../components/home/adminHome.component"
import BossHome from "../components/home/bossHome.component"
import EmployeeHome from "../components/home/employeeHome.component"
import Login from "../components/auth/login.component"
import Register from "../components/auth/register.component"
import ManageEmployees from "../components/EmployeeComponents/manageEmployees.component"
import ManagePlans  from "../components/PlansComponents/managePlans.component"
import SeeRegistro from "../components/RegistrosComponents/registro.component"
//CustomRoutes nos permite segregar nuestras rutas y bloquearlas dependiendo de factores booleanos como si un 
// es del tipo permitido o si esta logeado o no.
const CustomRoutes = ({options:options,component: Component, ...rest})=>{
    //genearmos dos tipos de rutas, las privadas y las restringidas. Las privadas solo son accesibles si se esta logeado,
    // las restringidas dependen del tipo de usuario logeado.
    let routeTypes = {
        private:  <Route {...rest} render={props => (options.isAuth 
                                                      ?<Component {...props} /> 
                                                      : <Redirect to={`/${options.redirect}`}/>)} />,
        restricted: <Route {...rest} render={props => (options.isAuth 
                                                        ?<Redirect to={`/${options.redirect}`} /> 
                                                        :<Component {...props} />)} />
    } 
    return routeTypes[options.elementType]
}
/* Nuestro comoponente router usara el React Hooks para saber si el usuario esta logeado y de que tipo es */
const BaseRouter = () =>{
    let isAuth = useSelector(state => state.AUTH_REDUCER.currentUser) //Seleccionamos del Reducer el estado del usuario
    let user_type = useSelector(state => state.AUTH_REDUCER.user_type)//Seleccionamos del Reducer el estado del tipo usuario
    let home = {null:NoLogedHome,Jefe:BossHome,Admin:AdminHome,Empleado:EmployeeHome}
    return (
        <React.Fragment>
            {/* Depende de tipo de usuario cargaremos un componente de home u otro */}
            <Route exact path='/' component={home[user_type]}/> 
            {/* Si el usuario ya se encuentra logeado tiene prohibido entrar a estas rutas */}
            <CustomRoutes options={{elementType:"restricted",redirect:"",isAuth:isAuth}} exact path='/login' component={Login}/>
            <CustomRoutes options={{elementType:"restricted",redirect:"",isAuth:isAuth}} exact path='/register' component={Register}/>
            {/* Bloqueamos las rutas si el usuario no pertenece al tipo indicado */}
            { <CustomRoutes options={{elementType:"restricted",redirect:"",isAuth:user_type==="Jefe" || user_type==="Admin" ?false:true}} 
                            exact path='/manage_employees' component={ManageEmployees}/>}
            { <CustomRoutes options={{elementType:"restricted",redirect:"",isAuth:user_type==="Jefe"?false:true}} 
                            exact path='/manage_plans' component={ManagePlans}/>}
            <CustomRoutes options={{elementType:"restricted",redirect:"",isAuth:user_type==="Jefe" || user_type==="Admin" ?false:true}} 
                            exact path='/manageRegistros/:user' component={SeeRegistro}/>
            
        </React.Fragment>
    )
}



export default BaseRouter