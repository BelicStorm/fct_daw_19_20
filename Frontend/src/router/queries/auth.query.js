import gql from 'graphql-tag';


const CreateCompany = gql`
mutation CreateCompany($empresa_name:String!, $account_type:String!){
    registerNewEmpresa(nombre:$empresa_name,account_type:$account_type){
      succes,
      message,
      empresa{
        id
      }
    }
  }
`
const CreateUserTable = gql`
mutation CreateUserTable($created_empresa:String!){
    createUserTable(table_name:$created_empresa){
      succes,
      message
    }
  }
`
const CreatePlanTable = gql`
mutation createPlanTable($empresa:String!){
  createPlanTable(empresa:$empresa){
    succes,
    message
  }
}

`

const insertUser = gql`
mutation InsertUser($email:String!,$password:String!,$nombre:String!,
                    $id_empresa:Int!,$empresa_name:String!,$user_type:String!,$plan_id:Int!){
    insertUser(email:$email,password:$password,nombre:$nombre,
              id_empresa:$id_empresa,empresa:$empresa_name,user_type:$user_type,plan_id:$plan_id){
        succes,
        message,
      }
  }
`
const login = gql`
    query Login($email:String!,$empresa:String!,$password:String!){
        login(email:$email, password:$password, empresa:$empresa){
            succes,
            message,
            token,
            user_type,
            plan_id
        }
    }
`
export{login,CreateCompany,CreateUserTable,insertUser,CreatePlanTable}