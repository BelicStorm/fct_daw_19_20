import gql from 'graphql-tag';


const create_table = gql`
mutation createRegistroTable($empresa_name:String!, $user:String!){
    createRegistroTable(empresa:$empresa_name,user:$user){
        succes,
        message
      }
  }
`
const insert_registro = gql`
mutation insertRegistro(
    $id_plan:Int!,$Fecha:String!,$hora:String!,
    $entrada:Boolean!,$salida:Boolean!,$Ausencia:Boolean!,$AusenciaConfirmada:Boolean!,
    $Vacaciones:Boolean!,$VacacionesConfirmadas:Boolean!,$user:String!
){
  insertRegistro(insert_input:{
    id_plan:$id_plan,
    Fecha:$Fecha,
    hora:$hora,
    entrada:$entrada,
    salida:$salida,
    Ausencia:$Ausencia,
    AusenciaConfirmada:$AusenciaConfirmada,
    Vacaciones:$Vacaciones,
    VacacionesConfirmadas:$VacacionesConfirmadas,
  },user:$user){
    succes,
    message
  }
}

`
const fetch_registro = gql`
query getRegistro($empresa_name:String!, $user:String!){
    selectAllRegistros(user:$user,empresa_name:$empresa_name){
        id,
        id_plan,
        user_mail,
        plan{
          plan_name,
          horas_dia,
          hora_entrada,
          hora_salida,
          vacaciones,
        }
        fecha,
        hora,
        entrada,
        salida,
        ausencia,
        ausenciaconfirmada,
        vacaciones,
        vacacionesconfirmadas
      }
}

`
const fetch_all_registros = gql`
query selectPlanFromALlTablesOfAnEmpresa($empresa_name:String!){
  selectPlanFromALlTablesOfAnEmpresa(empresa_name:$empresa_name){
      user_mail,
      plan{
        plan_name,
        horas_dia,
        hora_entrada,
        hora_salida,
        vacaciones,
      }
      fecha,
      hora,
      entrada,
      ausencia,
      ausenciaconfirmada,
      vacaciones,
      vacacionesconfirmadas
    }
}
`

export {
    create_table,fetch_registro, insert_registro,fetch_all_registros
}