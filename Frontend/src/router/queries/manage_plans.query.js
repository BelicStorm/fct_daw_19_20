import gql from 'graphql-tag';

const insert_plan = gql`

mutation insertPlan( $plan_name:String!,$horas_dia:Int!,$vacaciones:Int!,$hora_entrada:String!,$hora_salida:String!,$empresa:String!){
    createPlan(plan:{
      plan_name:$plan_name,
      horas_dia:$horas_dia,
      vacaciones:$vacaciones,
      hora_entrada:$hora_entrada,
      hora_salida:$hora_salida,
    },empresa:$empresa){
      succes,
      message
    }
  }

`
const getPlanById = gql`
  query GetPlanById($empresa:String!,$plan_id:Int!){
    getPlanById(empresa:$empresa,plan_id:$plan_id){
      plan_name,
      horas_dia,
      vacaciones,
      hora_entrada,
      hora_salida
    }
  }
`
const get_all_plans = gql`
{
    getAllPlans(empresa:"${localStorage.getItem("user_Empresa")}"){
      id,
      empresa_name,
      plan_name,
      horas_dia,
      vacaciones,
      hora_entrada,
      hora_salida
    }
}
`

export {insert_plan,get_all_plans,getPlanById}