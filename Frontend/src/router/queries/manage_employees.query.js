import gql from 'graphql-tag';


const SelectAllEmployees = gql`
    {
        SelectAllUsers(empresa:"${localStorage.getItem("user_Empresa")}"){
            id,
            email,
            user_type,
            nombre,
            id_empresa,
            plan_id,
            plan{
                plan_name,
                horas_dia,
                vacaciones,
                hora_entrada,
                hora_salida,
            }
        }
    }
`
const SelectEmployeesByMail = gql`
query SelectUserByMail($email:String!){
    SelectUserByMail(email:"email2",empresa:"${localStorage.getItem("user_Empresa")}"){
      id
      email
      id_empresa
      nombre
      user_type,
      plan_id
      
    }
}
`

export{SelectAllEmployees,SelectEmployeesByMail}