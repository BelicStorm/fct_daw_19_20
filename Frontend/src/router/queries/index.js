import {login,CreateUserTable,CreateCompany,insertUser,CreatePlanTable} from "./auth.query"
import {SelectAllEmployees,SelectEmployeesByMail} from "./manage_employees.query"
import {insert_plan,get_all_plans,getPlanById} from "./manage_plans.query"
import {create_table,fetch_registro,insert_registro,fetch_all_registros} from "./employeeRegistros.query"

const queries = {
    auth:{
        login:login,
        CreateUserTable:CreateUserTable,
        CreatePlanTable:CreatePlanTable,
        CreateCompany:CreateCompany,
        insertUser:insertUser
    },
    manage_employees:{
        select_all:SelectAllEmployees,
        select_by_mail:SelectEmployeesByMail
    },
    manage_plans:{
        insert_plan:insert_plan,
        get_all_plans:get_all_plans,
        getPlanById:getPlanById
    },
    employeeRegistros:{
        create_table:create_table,
        fetch_registro:fetch_registro,
        insert_registro:insert_registro,
        fetch_all_registros:fetch_all_registros
    }
}

export default queries