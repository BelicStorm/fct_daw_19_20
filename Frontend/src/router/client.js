import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';

const cache = new InMemoryCache();
const BASE_URL = 'http://localhost:3000/graphql';
const httpLink = createHttpLink({
    uri: BASE_URL,
  });
  
const authLink = setContext((_, { headers }) => {
// get the authentication token from local storage if it exists
    const token = localStorage.getItem('token');
    // return the headers to the context so httpLink can read them
    return {
        headers: {
        ...headers,
        Authorization: token ? token : "",
        }
    }
});
const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: cache
});

export default client