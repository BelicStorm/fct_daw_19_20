import client from "./client"

const agent ={
    query: (variables,query) =>{
        return client.query({
          query: query,
          fetchPolicy: 'network-only',
          variables: variables,
        })
    },
    mutation: (variables,query) =>{
      return client.mutate({
        mutation: query,
        variables: variables,
      })
    },
  }
  
  
  export default agent