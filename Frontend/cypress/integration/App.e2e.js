function login(credentials){
    cy.visit('/');// visit 'baseUrl'
    cy.contains("Login").click() //Efectuamos click en el contenedor del string Login
    //Insertamos en los inputs con el id indicado los datos obtenidos por parametros
    cy.get("input#complex-form_empresa").type(credentials.empresaName)
    cy.get("input#complex-form_email").type(credentials.mail)
    cy.get("input#complex-form_password").type(credentials.password)
    //Obtenemos el boton que efectua el login y clickamos en el 
    cy.get("button").contains("Login").parent().click()

}

describe("Bad credentials Login",()=>{
    it("succesfully performs bad login action", ()=>{
        login({ /* Pasamos los datos que darian como resultado un mal login */
            empresaName:"badEmpresaName",
            mail:"test",
            password:"123"
        })
        /* Si cuando acaba la operacion la pagina sigue teniendo el string de login */
        cy.contains('Login')
        
    });
})
describe('Correct Login and Logout', () => {
    it('succesfully performs login action of a boss user', () => {
      /* Pasamos los datos que darian como resultado un buen login */
      login({
            empresaName:"SuperEmpresa",
            mail:"test",
            password:"123"
        })
        /* Si cuando acaba la operacion la  nueva pagina tiene el string de logout */
        cy.window().then((win) => { 
            cy.contains("LogOut")
          })
        
    });
    it("succesfully performs a Logout action", ()=>{
        /* Clicamos el contenedor del string Logout */
        cy.contains("LogOut").click()
    });
});