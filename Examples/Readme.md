## Demos 📖
### No corras si sólo tienes fuerzas para andar — Brandon Sanderson, Elantris

En estas demos vas a observar la evolución que ha sufrido el Backend y el Frontend de <Nombre> a lo largo de toda la fase de implementación.

Desde una primera aproximación a las tecnologias base del proyecto a una demo funcional y primitiva de los cimientos de toda la aplicación.

1. [Node.js//Typescript//Postgres Crud](nodejs_type_example/Readme.md)
2. [TypeORM Crud](node_type_typeORM/Readme.md)
3. [TypeORM//GraphQL//ApolloFederation Crud](Graphql_test_implementation/Readme.md)