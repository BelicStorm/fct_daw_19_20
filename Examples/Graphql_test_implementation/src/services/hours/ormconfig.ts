
import {createConnection} from "typeorm";
import dotenv from 'dotenv';
dotenv.config(); 

export const connection = createConnection({
    name: "default",
    type: "postgres",
    host: "postgres",
    port: 5432,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    synchronize: true,
    logging: false,
    entities: [
        "dist/models/entity/*.js"
    ],
    subscribers: [
        "dist/models/subscriber/*.js"
    ],
    migrations: [
        "dist/models/migration/*.js"
    ],
    cli: {
        entitiesDir: "dist/models/entity",
        migrationsDir: "dist/models/migration",
        subscribersDir: "dist/models/subscriber"
    }
});



