import { gql } from "apollo-server-express";

const typeDefs = gql`

type history @key(fields: "id") {
  id: ID!
  workedHours: String,
  user: User
}

extend type User @key(fields: "id") {
  id: ID! @external
}


type Query {
  history: [history]
}
`;

export {
    typeDefs
}