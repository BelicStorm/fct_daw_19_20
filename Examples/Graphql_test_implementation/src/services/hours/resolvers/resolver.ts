import {History} from "../../../models/entity/history"
import {getRepository} from "typeorm"

const get = async () =>{
    const  response = await getRepository(History).find({ relations: ["user"] });  
    return response
}

const resolvers = {
  Query: {
    history(){
      return get()
    } 
  },
  history: {
    user(review:History) { 
      return { __typename: "User", id:review.user.id};
    } 
  },
  
 

  };


export {
    resolvers
}
