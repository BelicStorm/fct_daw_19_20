import { gql } from "apollo-server-express";

const typeDefs = gql`
extend type Query {
  AllUsers: [User],
  me(id: Int!): User

}

type Mutation {
  post(username: String!,email: String!): User! ,
  update(id: Int!, username: String, email: String): User!,
  delete(id: Int!): String
}


type User @key(fields: "id") {
  id: ID!
  username: String,
  email: String,
}

`;

export {
    typeDefs
}