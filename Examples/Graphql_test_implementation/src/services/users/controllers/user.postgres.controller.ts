import {getRepository} from "typeorm"
import { UserTest2 } from "../../../models/entity/User"

const getUsers = async () =>{
    const  response = await getRepository(UserTest2).find();
    return response
}
const getUsersById = async (id:string) =>{
  const  response = await getRepository(UserTest2).findOne(id);
  return response
}
const createUser = async (data:object) =>{
  const newData =  getRepository(UserTest2).create(data);
  const response = await getRepository(UserTest2).save(newData);
  return response
}
const deleteById = async (id:string) =>{
  await getRepository(UserTest2).delete(id);
  return "Deleted"
}
const updateUser = async (id:number,data:object) =>{
  const request =  await getRepository(UserTest2).findOne(id);
  if(request){
      getRepository(UserTest2).merge(request,data);
      const response = await getRepository(UserTest2).save(request);
      return response
  }
  
  
}
export {
    getUsers,getUsersById,createUser,deleteById,updateUser
}