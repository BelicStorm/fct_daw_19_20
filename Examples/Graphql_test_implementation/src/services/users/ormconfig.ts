
import {createConnection} from "typeorm";

export const connection = createConnection({
    name: "default",
    type: "postgres",
    host: "postgres",
    port: 5432,
    username: "BVN_user",
    password: "bvn_password",
    database: "test_database",
    synchronize: true,
    logging: false,
    entities: [
        "dist/models/entity/*.js"
    ],
    subscribers: [
        "dist/models/subscriber/*.js"
    ],
    migrations: [
        "dist/models/migration/*.js"
    ],
    cli: {
        entitiesDir: "dist/models/entity",
        migrationsDir: "dist/models/migration",
        subscribersDir: "dist/models/subscriber"
    }
});

