import "reflect-metadata"
import { ApolloServer } from "apollo-server-express";
import  { buildFederatedSchema } from '@apollo/federation';
/* import { ApolloGateway } from "@apollo/gateway"; */
import {connection} from "./ormconfig"
import {resolvers} from "./resolvers/resolver"
import {typeDefs} from "./typeDefs/typeDefs"

import express from "express";


export async function listen(port: number) {
      await connection

      const app = express()
      const schema = buildFederatedSchema({
        typeDefs,
        resolvers
      });
    
      const server = new ApolloServer({
        schema,
        tracing: false,
        playground: true,
      });
      server.applyMiddleware({app})
         app.listen(port, () =>
          console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)
        );
    
      return server.graphqlPath;
}
