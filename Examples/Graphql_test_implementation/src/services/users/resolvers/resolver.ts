import {createUser,deleteById,getUsers,getUsersById,updateUser} from "../controllers/user.postgres.controller"
import { UserTest2 } from "../../../models/entity/User";

const resolvers = {
  Query: {
    AllUsers() {
      return getUsers()
    },
    me(_obj:any, args:any, _context:any, _info:any) {
      return getUsersById(args.id);
    },
  },
  Mutation: {
    // 2
    post: (_parent:any, args:any) => {
      return createUser({username:args.username,email:args.email})
    },
    update(_parent:any, args:any) {
      return updateUser(args.id,{username:args.username,email:args.email})
    },
    delete(_parent:any,args:any) {
      return deleteById(args.id)
    }
  },
  User: {
    __resolveReference(object:any) {
      return getUsersById(object.id)
    }
  }
  
};



export {
    resolvers
}