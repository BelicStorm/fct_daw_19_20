import {Entity, Column, PrimaryGeneratedColumn,JoinColumn,OneToOne} from "typeorm"
import {UserTest2} from "./User"

@Entity()
export class History {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    workedHours:string

    @OneToOne(_type => UserTest2, user => user.id)

    @JoinColumn()
    user: UserTest2;

}