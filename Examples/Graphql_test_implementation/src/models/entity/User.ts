import {Entity, Column, PrimaryGeneratedColumn} from "typeorm"

@Entity()
export class UserTest2 {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    username:string
    @Column()
    email:string
}
