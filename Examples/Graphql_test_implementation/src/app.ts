import "reflect-metadata"
import express from 'express';
import { ApolloServer } from 'apollo-server-express'
import { ApolloGateway } from '@apollo/gateway'
import {listen as users_listen} from "./services/users"
import {listen as history_listen} from "./services/hours"

const init = async () => {
  const gateway = new ApolloGateway({
    serviceList: [
      { name: 'example', url: `http://localhost:3999${await users_listen(3999)}` },
      { name: 'history', url: `http://localhost:4000${await history_listen(4000)}` },
    ],
  })
  const app = express();
  const { schema, executor } = await gateway.load()
  const server = new ApolloServer(
    { 
      schema, 
      executor
     })
  server.applyMiddleware({app})
  app.listen(3000, () =>
       console.log(`🚀 Server ready at http://localhost:3000${server.graphqlPath}`)
    );
}
init()
