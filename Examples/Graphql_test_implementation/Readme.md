# Instalación 🔧
En la última demo ya implementamos todas las tecnologías restantes: Graphql y Apollo Federation. Ademas testeamos el sitema de relationships que nos
ofrece TypeORM.

Antes de todo clonaremos el repositorio. Ya clonado instalaremos seguiremos los siguientes pasos:

## .env

Editaremos los datos de nuestra base de datos postgres:

        POSTGRES_USER=user
        POSTGRES_PASSWORD=password
        POSTGRES_DB=database

## Puesta en marcha

Ya configurada nuestra base de datos resta iniciar nuestros servicios con 

```
    docker-compose up
```
<img src="../../media/demo3/puesta_en_marcha.png"  width="700" height="400" style="  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" />

## Puesta en marcha

Encendidos ya nuestros servicios resta ver como consumirlos: 

Primero de todo observaremos el esquema que nos genera y por ende como llegaremos a tratarlo, en un futuro, en el frontend:

<img src="../../media/demo3/esquema.png"  width="700" height="400" style="  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" />

Resta ver como funciona:
  <img src="../../media/demo3/funcionamiento2.gif"  width="700" height="400" style="  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" />


