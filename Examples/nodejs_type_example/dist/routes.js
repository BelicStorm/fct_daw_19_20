"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const controllers_1 = require("./sample_crud/controllers.");
const user_controllers_1 = require("./typeOrmCrud/controllers/user.controllers");
const router = express_1.Router();
router.get("/users", controllers_1.getUsers);
router.get("/users/:id", controllers_1.getUsersById);
router.post("/users", controllers_1.createUser);
router.delete("/users/:id", controllers_1.deleteUsersById);
router.put("/users/:id", controllers_1.updateUserById);
router.get("/orm/users", user_controllers_1.ormGetUser);
/* router.get("/orm/users/:id",getUsersById)
router.post("/orm/users",createUser)
router.delete("/orm/users/:id",deleteUsersById)
router.put("/orm/users/:id",updateUserById) */
exports.default = router;
