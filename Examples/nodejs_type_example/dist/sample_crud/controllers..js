"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("./database"));
const getUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield database_1.default.query("SELECT * from test_users");
        console.log(response.rows);
        return res.status(200).json(response.rows);
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`);
    }
});
exports.getUsers = getUsers;
const getUsersById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const response = yield database_1.default.query("SELECT * from test_users where id = $1", [id]);
        console.log(response.rows);
        return res.status(200).json(response.rows);
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`);
    }
});
exports.getUsersById = getUsersById;
const createUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name, mail } = req.body;
        yield database_1.default.query("INSERT INTO test_users (user_name, user_mail) VALUES ($1,$2)", [name, mail]);
        return res.status(200).json({
            message: "User Created Successfully"
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`);
    }
});
exports.createUser = createUser;
const deleteUsersById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        yield database_1.default.query("DELETE from test_users where id = $1", [id]);
        return res.status(200).json({
            message: `User with id: ${id} was deleted`
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`);
    }
});
exports.deleteUsersById = deleteUsersById;
const updateUserById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const { name, email } = req.body;
        yield database_1.default.query("UPDATE test_users SET user_name = $1, user_mail = $2 where id = $3", [name, email, id]);
        return res.status(200).json({
            message: "User Updated Successfully"
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`);
    }
});
exports.updateUserById = updateUserById;
