"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const express_1 = __importDefault(require("express"));
const routes_1 = __importDefault(require("./routes"));
/* import cors from "cors" */
const morgan_1 = __importDefault(require("morgan"));
const typeorm_1 = require("typeorm");
typeorm_1.createConnection().then((connection) => __awaiter(void 0, void 0, void 0, function* () {
    const app = express_1.default();
    //middlewares
    /* app.use(cors()); */
    app.use(morgan_1.default("dev"));
    app.use(express_1.default.json()); //interprets the json data
    app.use(express_1.default.urlencoded({ extended: false })); // if the client send to the server data from a form transform it to an json object
    //router
    app.use(routes_1.default);
    //server
    let port = 3000;
    app.listen(port);
    console.log(`server on port ${port}`);
})).catch(error => console.log(error));
