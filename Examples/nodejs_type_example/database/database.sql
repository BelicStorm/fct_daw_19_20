DROP DATABASE IF EXISTS test_database;
CREATE DATABASE IF NOT EXISTS test_database;
\c test_database

DROP TABLE IF EXISTS test_users;
CREATE TABLE IF NOT EXISTS test_users(
    id  SERIAL  PRIMARY KEY,
    user_name   VARCHAR(40),
    user_mail   TEXT
);
INSERT INTO test_users (user_name,user_mail) 
        VALUES ('test','mail1'),
               ('test2','mail2');