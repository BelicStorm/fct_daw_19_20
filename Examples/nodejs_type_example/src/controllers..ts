import { Request, Response} from "express"
import { QueryResult } from "pg"
import pool from "./database"

const getUsers = async (req: Request, res: Response): Promise<Response> =>{
    try {
        const response: QueryResult = await pool.query("SELECT * from test_users")
        console.log(response.rows);
        
        return res.status(200).json(response.rows)
    } catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`)
    }

}
const getUsersById = async (req: Request, res: Response): Promise<Response> =>{
    try {
        const id = parseInt(req.params.id)
        const response: QueryResult = await pool.query("SELECT * from test_users where id = $1",[id])
        console.log(response.rows);
        
        return res.status(200).json(response.rows)
    } catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`)
    }

}
const createUser = async (req: Request, res: Response): Promise<Response> =>{
    try {
        const {name, mail} = req.body
        await pool.query("INSERT INTO test_users (user_name, user_mail) VALUES ($1,$2)",[name,mail])
        return res.status(200).json(
            {
                message:"User Created Successfully"
            }
        )
    } catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`)
    }

}
const deleteUsersById = async (req: Request, res: Response): Promise<Response> =>{
    try {
        const id = parseInt(req.params.id)
        await pool.query("DELETE from test_users where id = $1",[id])
   
        return res.status(200).json({
            message:`User with id: ${id} was deleted`
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`)
    }

}
const updateUserById = async (req: Request, res: Response): Promise<Response> =>{
    try {
        const id = parseInt(req.params.id)
        const {name, mail} = req.body
        await pool.query("UPDATE test_users SET user_name = $1, user_mail = $2 where id = $3",[name,mail,id])
        return res.status(200).json(
            {
                message:"User Updated Successfully"
            }
        )
    } catch (error) {
        console.log(error);
        return res.status(500).json(`Internal server error`)
    }

}
export {
    getUsers,getUsersById,createUser, deleteUsersById,updateUserById
}
