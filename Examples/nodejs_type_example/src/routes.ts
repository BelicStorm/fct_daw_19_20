import {Router} from 'express'
import {getUsers,getUsersById,createUser,deleteUsersById,updateUserById } from './controllers.'
const router = Router();

router.get("/users",getUsers)
router.get("/users/:id",getUsersById)
router.post("/users",createUser)
router.delete("/users/:id",deleteUsersById)
router.put("/users/:id",updateUserById)
 

export default router; 