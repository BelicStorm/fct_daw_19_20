import {Pool} from "pg"
import dotenv from 'dotenv';

dotenv.config(); 

const pool = new Pool ({
    user: process.env.POSTGRES_USER,
    host: "postgres",
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    port:5432
})

export default pool;