import React from "react"


class UserRelatedTestForm extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        email:"",
        nombre:"",
        password:"",
        user_type:""
      }
      this.onInputChanges = this.onInputChanges.bind(this);
  }

    onInputChanges(e){
      console.log(e.target.name)
      this.setState({...this.state, [e.target.name]:e.target.value})
      console.log(this.state)
    }
    makeForm(){
      switch (this.props.form) {
        case "SelectUser":
          return (
           <div id={`${this.props.prefix}-SelectUserBy`}>
             <input className="Select-form-control" name="email" type="text" placeholder="Enter email" onChange={this.onInputChanges}></input>
             <input type="button" id="Select-form-submit" defaultValue="Select" onClick={
                        ()=>this.props.onSubmit(this.state)
              }></input>
           </div>
          );
       case "InsertUser":
        return (<div id={`${this.props.prefix}-InsertUser`}>
          <input className="Insert-form-control" name="email" type="text" placeholder="Enter email" onChange={this.onInputChanges}></input>
          <input className="Insert-form-control" name="nombre" type="text" placeholder="Enter name" onChange={this.onInputChanges}></input>
          <input className="Insert-form-control" name="password" type="text" placeholder="Enter password" onChange={this.onInputChanges}></input>
          <input className="Insert-form-control" name="user_type" type="text" placeholder="Enter user_type" onChange={this.onInputChanges}></input>
          <input type="button" id="Insert-form-submit" defaultValue="Insert" onClick={
                    ()=>this.props.onSubmit(this.state)
          }></input>
        </div>)
          
       /*   case "DeleteUser":
      
          break;
      
        default:
          break; */
      }
    }
    render() {
      return this.makeForm()
    }
  }
export default UserRelatedTestForm



