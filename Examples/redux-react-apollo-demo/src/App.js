import React from 'react';
import userRequest from "./router/userTest.agent"
import UserRelatedTestForm from "./componets/forms.component"
import {connect} from 'react-redux'
import testActions from "./duck/test.actions"

const mapStateToProps = (state) => {
  return {
      users: state.TEST_REDUCER.data.SelectAllUsers /* Los productos obtenidos del store transformados en props del componente */
  }
}
const mapDispatchToProps = dispatch =>({ 
  /* Creamos una funcion que contenga todas las acciones 
  necesarias que modifiquen el store a nuestro placer. En este caso llamaremos a las acciones del pato
  asociado a esta funcionalidad, el cual se encargara de efectuar toda la logica de la peticion al servidor y de lanzar
  el action que modificara el reducer */
  onLoad:()=>dispatch(testActions.SELECT())
})

class App extends React.Component  {
  constructor(props){
    super(props);
    this.state = {
      loadedUsers:false,
      selectByTest:"",
      error:false,
      errormessage:"",
      insertFlag:false,
      insertMessage:"",
      loadingAllUsers:true,
      allUsers:""
    }
    this.onSubmitSelectBy = this.onSubmitSelectBy.bind(this);
    this.insertUser = this.insertUser.bind(this);
    this.props.onLoad()
  }
  onSubmitSelectBy(e){
    userRequest.selectUserBy(e.email).then(res=>{
      if(res.data.SelectUserByMail[0]){
        this.setState({...this.state, loadedUsers:true,selectByTest:res.data.SelectUserByMail[0],error:false})
      }else{
        this.setState({...this.state, loadedUsers:true,selectByTest:"",error:true,errormessage:`There are no users with ${e.email} mail`})
      }
    }).catch(err=>{
      console.log(err);
      this.setState({...this.state, loadedUsers:true,selectByTest:"",error:true,errormessage:err})
    })
    
  }
  insertUser(e){
    userRequest.insertUser(e).then(res=>{
      console.log(res.data.insertUser);
      if(res.data.insertUser.succes){
        console.log(res);
        this.setState({...this.state, insertFlag:true, insertMessage:res.data.insertUser.message})
      }
    }).catch(err=>{
      console.log(err);
    })
  }
  deleteUser(mail){
    userRequest.deleteUser(mail).then(res=>{
      console.log(res);
      if (res.data.deleteUser.succes) {
          window.location.reload()
      }
      
    }).catch(err=>{
      console.log(err);
    })
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps!==undefined) {
      return true
    }
    if(nextState.loadedUsers === true){
      this.setState({...this.state, loadedUsers:false})
      return true
    }
    if(nextState.error === true){
      this.setState({...this.state, loadedUsers:false,error:false})
      return true
    }
    if(nextState.insertFlag ===true){
      this.setState({...this.state, insertFlag:false})
      return true
    }
    return false
  }
  render(){
    return (
      <div className="app">
        <h3>Select All Users</h3>
            <ul>
              {
                this.props.users === undefined ? <div>Loading</div> :
                this.props.users.map((user)=>{
                  return <li key={user.email}>{user.email}  <button onClick={()=>{this.deleteUser(user.email)}}>Delete User</button></li>
                })
              }
           </ul>

        <h3>Select User By mail</h3>
          <UserRelatedTestForm prefix="form1" form="SelectUser" onSubmit={this.onSubmitSelectBy}></UserRelatedTestForm>
            {this.state.errormessage ? <span>{this.state.errormessage}</span>:<></> }
            {this.state.loadedUsers ? 
              <span>{this.state.selectByTest.email}:{this.state.selectByTest.nombre}</span>
            :<></>}
          <h3>Insert Userl</h3>
          <UserRelatedTestForm prefix="form2" form="InsertUser" onSubmit={this.insertUser}></UserRelatedTestForm>
          {this.state.insertFlag ? 
             <span>{this.state.insertMessage}</span>
            :<></>}
      </div> 
    );
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App)
