import { createStore, applyMiddleware, compose } from 'redux'
import duckOperations from './operations'
import reducer from './test.reducer'
/* En este fichero crearemos el store de toda la aplicación. */

/* Primero de todo indicaremos que devtools usaremos en nuestro navegador */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

/* Luego crearemos la constante encargada de almacenar nuestro store. Le indicaremos
los reducers de los que va a estar formado, que no tendrá estado inicial y que middleware va a controlar las llamadas*/
const store = createStore(reducer,undefined,
    composeEnhancers(applyMiddleware(duckOperations))
)
export default store