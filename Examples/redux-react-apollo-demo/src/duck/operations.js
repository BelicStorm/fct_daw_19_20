import userRequest from "../router/userTest.agent"
const duckOperations = store => next => action => {
    if(action.method !== undefined){    /* Si la accion tiene un metodo asignado llamaremos al servidor */
          store.dispatch({type:action.type+"_PENDING"})  
      userRequest[action.method](action.variable).then(
        res=>{
          /* Si la peticion ha sido correcta lanzaremos la accion anterior pero ahora con el sufijo de _SUCCESS */
          store.dispatch({type:action.type+"_SUCCESS",data:res.data}) 
        },
        err=>{
          /* Si la peticion ha sido erronea lanzaremos la accion anterior pero ahora con el sufijo de _FAILURE */
          store.dispatch({type:action.type+"_FAILURE",error:err})
        })
        return;
    }
    /* Si la accion no necesita de una logica en el servidor la pasaremos directamente al REDUCER */
    next(action)

 }
export default duckOperations