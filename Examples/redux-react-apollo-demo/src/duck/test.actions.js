import {types} from "./test.types"

const testActions ={
    SELECT: ()=>{ /* Estado consecuencia de la llamada al servidor, lanzado unicamente por operations.js */
        console.log("Select success");
        return {type: types.SELECT,method:"selectAll"}},
    SELECT_SUCCESS: ()=>{ /* Estado consecuencia de la llamada al servidor, lanzado unicamente por operations.js */
        console.log("Select success");
        return {type: types.SELECT_SUCCESS}},
    SELECT_FAILURE: ()=>{ /* Estado consecuencia de la llamada al servidor, lanzado unicamente por operations.js */
        console.log("Select fail");
        return {type: types.SELECT_FAILURE}},
    SELECT_PENDING: ()=>{ /* Estado consecuencia de la llamada al servidor, lanzado unicamente por operations.js */
        console.log("Select pending");
        return {type: types.SELECT_PENDING}},

}

export default testActions