/* REDUCER(S)
En este fichero crearemos todos los reducers encargados de modificar el pato y usaremos el combinador de reducers que nos ofrece
redux para exportar al store esos reducers conbinados en uno.

Cada reducer tendra un initial_state en el que daremos forma a los posibles cambios en el estado de cada reducer.

En el return de cada action devolveremos el state modificado usando solo los parametros establecidos en el initial state de
cada reducer. En el caso de necesitar una logica de cambio mas elevada usaremos los selectores para llamar a las funciones que 
se encarguen de dicha tarea.
*/
import { combineReducers } from "redux";
import {types} from "./test.types";

//State Shape
const test_state ={
    loading_fetch:false,
    error:false,
    data:[]
}
const test_reducer = ( state = test_state, action ) => {
    const test_reducer_cases = {
        SELECT:{loading_fetch:true,error:false},
        SELECT_PENDING:{loading_fetch:true,error:false},
        SELECT_SUCCESS:{...state, loading_fetch:false,data: action.data},
        SELECT_FAILURE:{...state, loading_fetch:false,error: action.error}
    }
    return test_reducer_cases[action.type]===undefined ? test_state : test_reducer_cases[action.type]
}

const reducer = combineReducers( {
    TEST_REDUCER: test_reducer,
} );

export default reducer;