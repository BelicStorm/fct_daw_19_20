import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import store from "./duck/store"
import { ApolloProvider } from 'react-apollo';
import client from "./router/client"
import App from './App';
import ErrorBoundary from "./errorBoundary"

ReactDOM.render(
  <Provider store={store}>
    <ApolloProvider client={client}>
      <ErrorBoundary>
        <App />
      </ErrorBoundary>
    </ApolloProvider>
  </Provider>,
  document.getElementById('root'),
);
