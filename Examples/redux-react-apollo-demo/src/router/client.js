
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

const cache = new InMemoryCache();

const BASE_URL = 'http://localhost:3000/graphql';
const httpLink = new HttpLink({
  uri: BASE_URL,
  headers: {
    Authorization: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImVtYWlsMiIsInVzZXJfdHlwZSI6IkplZmUiLCJpYXQiOjE1ODc0ODE2MzR9.VJ6lg0th3CE6vnFXIaW5x8DpWcIHCEnDWkDLBejjNV4`,
  },
});
const client = new ApolloClient({
  link: httpLink,
  cache,
});

export default client