import gql from 'graphql-tag';
import client from "./client"

const selectUserBy = gql`
query SelectUserByMail($email: String!) {
    SelectUserByMail(email:$email,empresa:"NuevaEmpresa3"){
        id,
        email,
        id_empresa,
        nombre,
        contrasena,
        user_type,
        empresa{
            id
            nombre
            account_type
    }
  }
}
`;
const insertUser = gql`
mutation InsertUser($email: String!,$password: String!,$nombre: String!,$user_type: String!) {
  insertUser(email:$email,password:$password,nombre:$nombre,id_empresa:8,empresa:"NuevaEmpresa3",user_type:$user_type){
    succes,
    message
  }
}
`;
const selectAll = gql`
{
  SelectAllUsers(empresa:"NuevaEmpresa3"){
    email
  }
}

`;
const deleteUser = gql`
mutation deleteUser($userMail:String!){
  deleteUser(userMail:$userMail,empresa:"NuevaEmpresa3"){
    succes,
    message
  }
}
`

const userRequest ={
  selectUserBy: (email) =>{
      return client.query({
        query: selectUserBy,
        fetchPolicy: 'network-only',
        variables: { email }
      })
  },
  selectAll: () =>{
      return client.query({
        query: selectAll,
        fetchPolicy: 'network-only',
      })
  },
  insertUser:(variables)=>{
    return client.mutate({
      mutation: insertUser,
      variables: variables
    })
  },
  deleteUser:(variables)=>{
    return client.mutate({
      mutation:deleteUser,
      variables:{userMail:variables}
    })
  }
}


export default userRequest