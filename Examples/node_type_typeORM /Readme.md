# Instalación 🔧
En esta demo implementamos el ORM TypeORM sobre la primera demo. El objetivo es reducir el código que se pueda llegar a repetir y simplificar las sentencias preparadas para evitar SQL Injection sobre nuestra base de datos.

Antes de todo clonaremos el repositorio. Ya clonado instalaremos seguiremos los siguientes pasos:

## .env

Editaremos los datos de nuestra base de datos postgres:

        POSTGRES_USER=user
        POSTGRES_PASSWORD=password
        POSTGRES_DB=database

## Puesta en marcha

Ya configurada nuestra base de datos resta iniciar nuestros servicios con 

```
    docker-compose up
```
<img src="../../media/demo2/docker-compose_up.png"  width="700" height="400" style="  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" />

## Puesta en marcha

Encendidos ya nuestros servicios resta ver como consumirlos: 
(El ejemplo esta realizado con [PostMan](https://www.postman.com/))

<img src="../../media/demo2/funcionamiento.gif"  width="700" height="400" style="  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" />
