import {Entity, Column, PrimaryGeneratedColumn} from "typeorm"

@Entity()
export class UserTest2 {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    user_name:string
    @Column()
    user_mail:string
}