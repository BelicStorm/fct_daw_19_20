import {Router} from "express"
const router = Router()
import {getUsers,createUser,getUsersById,deleteById,updateUser} from "./controllers/user.controller"

router.get("/users",getUsers); //get all users
router.get("/users/:id",getUsersById); //get users by id
router.post("/users",createUser); //create users
router.put("/users/:id",updateUser); //update users by id
router.delete("/users/:id",deleteById); //delete users by id

export default router