import "reflect-metadata"
import {createConnection} from "typeorm";
import * as express from 'express';
import router from "./routes"

// connection settings are in the "ormconfig.json" file
createConnection().then(async connection => {
    const app = express();

    //middlewares
    app.use(express.json()) //interprets the json data
    app.use(express.urlencoded({extended:false})) // if the client send to the server data from a form transform it to an json object

    app.use(router) 


    let port = 3000;

    app.listen(port)

    console.log(`server on port ${port}`)

}).catch(error => console.log("Error: ", error));