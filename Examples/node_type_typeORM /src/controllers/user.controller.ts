import { Request, Response} from "express"
import {getRepository} from "typeorm"
import { UserTest2 } from "../entity/User"

const getUsers = async (req: Request, res: Response): Promise<Response> =>{
    const response = await getRepository(UserTest2).find();
    return res.status(200).json(response);
}
const getUsersById = async (req: Request, res: Response): Promise<Response> =>{
    const id = req.params.id
    const response = await getRepository(UserTest2).findOne(id);
    return res.status(200).json(response);
}
const deleteById = async (req: Request, res: Response): Promise<Response> =>{
    const id = req.params.id
    const response = await getRepository(UserTest2).delete(id);
    return res.status(200).json(response);
}
const createUser = async (req: Request, res: Response): Promise<Response> =>{
    const newData =  getRepository(UserTest2).create(req.body);
    const response = await getRepository(UserTest2).save(newData);
    return res.status(200).json(response);
}
const updateUser = async (req: Request, res: Response): Promise<Response> =>{
    const request =  await getRepository(UserTest2).findOne(req.params.id);
    if(request){
        getRepository(UserTest2).merge(request,req.body);
        const response = await getRepository(UserTest2).save(request);
        return res.status(200).json(response);
    }else{
        return res.status(500).json({error:"no user has found with this id"});
    }
    
    
}

export {
    getUsers,createUser,getUsersById,deleteById,updateUser
}