interface select{
    table:string,
    columns: string | string[]
}
interface update{
    table:string,
    arguments:{
        column:string,
        argument:string
    },
    moreArgumentsBool:boolean,
    more_arguments?:[{
        column:string,
        argument:string
    }],
    where:where_and_or
    
}
interface where_and_or {
    column:string,
    condition:string,
    conditionalnumber?:string,
    value:string,
    previous?:string
}

interface user {
    id?:number,
    email:string,
    password:string,
    user_type?:string
    nombre?:string,
    id_empresa?:number
}
interface auth_result_message {
    succes:boolean,
    message:string,
    token?:string,
}
interface empresa {
    id?:number,
    nombre:string,
    account_type:string
}

interface Plan {
    id?:number,
    plan_name:string,
    id_user:number,
    horas_dia:number,
    vacaciones:number,
    hora_entrada:string,
    hora_salida:string
}
interface select_where_and_or {
    select:{
        column:string,
        from:string
    },
    where_create:boolean,
    conditionals_create:boolean,
    where?:{
        column:string,condition?:string,conditionalnumber?:string,
        value:any,previous?:string
    },
    conditionals?:{
        option:string[],
        columns:string[],conditions?:string[],conditionalnumber?:string[],
        values:any[],previous?:string[]
    }

}
interface registro {
    id?:number,
    id_plan:number,
    id_admin:number,
    Fecha:string,
    hora:string,
    entrada:boolean,
    salida:boolean,
    Ausencia:boolean,
    AusenciaConfirmada:boolean,
    Vacaciones:boolean,
    VacacionesConfirmadas:boolean
}
export {
    user,auth_result_message,empresa,update,Plan,select,where_and_or,select_where_and_or,registro
}