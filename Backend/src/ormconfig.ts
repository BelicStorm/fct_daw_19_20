import {createConnection} from "typeorm";
import dotenv from 'dotenv';
dotenv.config(); 

export const connection = createConnection({
    name: "default",
    type: "postgres",
    host: "postgres",
    port: 5432,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    synchronize: true,
    logging: false,
    entities: [
        "dist/model/TypeORM/entity/*.js"
    ],
    subscribers: [
        "dist/model/TypeORM/subscriber/*.js"
    ],
    migrations: [
        "dist/model/TypeORM/migration/*.js"
    ],
    cli: {
        entitiesDir: "dist/model/TypeORM/entity",
        migrationsDir: "dist/model/TypeORM/migration",
        subscribersDir: "dist/model/TypeORM/subscriber"
    }
});



