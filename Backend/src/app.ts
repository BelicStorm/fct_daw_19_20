import "reflect-metadata"
import express from 'express';
import { ApolloServer } from 'apollo-server-express'
import { ApolloGateway, RemoteGraphQLDataSource } from '@apollo/gateway'
import {user_listen,empresa_listen,plan_listen, registro_listen} from "./services"
import helmet from "helmet"
import cors from "cors"
import Utils from "./utils/utils";

/**
* Personalizar el transporte de datos de la puerta de enlace todos los servicios federados
* @namespace InheritData
* @method willSendRequest
* @param {Object} { request, context } 
*/
class InheritData extends RemoteGraphQLDataSource {  
  //willSendRequest nos permite interceptar la request antes de que llegue al servicio y en este caso modificar las cabeceras que estos van a utilizar.
  willSendRequest({ request, context }) {  
    request.http.headers.set('token', context.token);
    request.http.headers.set('email', context.email);  
    request.http.headers.set('user_type', context.user_type);    
  }
}

const init = async () => {
  //Creamos la puerta de enlace con los servicios federados
  const gateway = new ApolloGateway({
    serviceList: [
      { name: 'empresa', url: `http://localhost:4000${await empresa_listen(4000) }` },
      { name: 'user', url: `http://localhost:3999${await user_listen(3999) }` },
      { name: 'plan', url: `http://localhost:3998${await plan_listen(3998) }` },
      { name: 'registros', url: `http://localhost:3997${await registro_listen(3997) }` },
      
    ],
    buildService({ name, url }) {    return new InheritData({ url });  },//Servicio que hara posible que los servicios federados hereden del contexto principal
  })

  //Creamos el servidor Express y sus middlewarse
  const app = express();
  app.use(helmet())
  app.use(cors())
  app.use(express.urlencoded({extended: true}))

  //Cargamos la puerta de enlace de apollo y obtenemos de ella el servicio que ejecuta los esquemas federados y el esquema entero
  const { schema, executor } = await gateway.load()

  //Creamos el servidor apollo y añadimos un contexto para conseguir de las cabeceras el token
  const server = new ApolloServer(
    { 
      schema, 
      executor,
      context: ({ req }) => {
        // get the user token from the headers
          return new Promise((resolve,reject)=>{
            const token = req.headers.authorization || '';
            
            Utils.jwtCompare(token).then((res:any)=>{
              resolve({token:token, email:res.email,user_type:res.user_type})
            }).catch(err=>{
              resolve({})
            })
          })

      },
     })
  
  server.applyMiddleware({app})
  app.listen(3000, () =>
       console.log(`🚀 Server ready at http://localhost:3000${server.graphqlPath}`)
    );
}
init()
