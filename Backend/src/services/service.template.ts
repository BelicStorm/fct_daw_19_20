import "reflect-metadata"
import { ApolloServer} from "apollo-server-express";
import  { buildFederatedSchema } from '@apollo/federation';
import {connection} from "../ormconfig"
import express, {Application} from "express";
import { GraphQLSchema } from "graphql";
import helmet from "helmet"
import cors from "cors"

interface props {
    typeDefs: any,
    resolvers: any,
    typeORMConfig:boolean
}

class FederatedService {
    app: Application //Guardamos la instancia del servidor express
    data:props
    //El constructor no genera la conexión a TypeOrm, en el caso de que el servicio 
    //la necesite, la instancia del servidor express y los datos necesarios para ponerlo en marcha.
    constructor(data:props){
       if(data.typeORMConfig){
        this.ORMConnection()
       }
        this.app = express()
        this.data = data
    }
//Generamos la conexión a TypeORM obteniendo la configuración de ormconfig.ts
    private async ORMConnection(){
        await connection;
    } 
//Generamos el esquema del servicio
    private async federatedSchema(typeDefs,resolvers){
       return buildFederatedSchema({
            typeDefs,
            resolvers
        });
    }
///Aplicamos los mismos middlewares que aplicamos en app.ts
    private async middlewares(server){
        const app = this.app
        app.use(helmet())
        app.use(cors())
        app.use(express.urlencoded({extended: true}))
        server.applyMiddleware({app})
    }
    async listen(port:Number){
    ///Pasamos por parámetros al método que genera los esquemas el typedef y los resolvers del servicio
        const schema:GraphQLSchema = await this.federatedSchema(this.data.typeDefs,this.data.resolvers);
    //Tal y como vimos en app.ts, tenemos configurado un contexto que los servicios tiene que recibir.
        const server = new ApolloServer({
            schema,
            tracing:true,
            playground: true,
            context: ({req}) => {
                // Get the user token from the headers.
                const token = req.headers.token || '';
                const email = req.headers.email || '';
                const user_type = req.headers.user_type || '';
                return {token,email,user_type} 
              }
          });
        await this.middlewares(server);
        this.app.listen(port, () =>
            console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)
        );
        return server.graphqlPath
    }
}
export default FederatedService