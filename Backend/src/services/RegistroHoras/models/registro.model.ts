import Model from '../../../model/database'
import {select_where_and_or, registro, update} from '../../../interfaces/intefarces'

class RegistroModel extends Model{

    selectBy(data:select_where_and_or){
        return new Promise((resolve,reject)=>{
            this.select({columns:data.select.column,table:data.select.from+""})
            if (data.where_create) {
                this.where({
                    column:data.where.column,condition:data.where.condition,conditionalnumber:'1',
                    value:data.where.value,previous:'SELECT'
                })
            }
            if(data.conditionals_create){
                for (let index = 0; index < data.conditionals.option.length; index++) {
                    if(data.conditionals.option[index]==='AND'){
                        this.and({
                            column:data.conditionals.columns[index],condition:data.conditionals.conditions[index],
                            conditionalnumber:`${index+1}`,value:data.conditionals.values[index],previous:'WHERE'
                        })
                    }else{
                        this.or({
                            column:data.conditionals.columns[index],condition:data.conditionals.conditions[index],
                            conditionalnumber:`${index+1}`,value:data.conditionals.values[index],previous:'WHERE'
                        })
                    }
                }
            }
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    createRegistroTable(empresa:string,user:any,){
       return new Promise((resolve,reject)=>{
/*             this.selectBy({
                select:{column:'*',from:empresa+'_user_table'},
                conditionals_create:false,
                where_create:true,
                where:{column:'id',value:user,condition:'='}
            }).then((res:any[])=>{
                if(res.length === 0){
                    reject(`The user with id: ${user} does not exist in the ${empresa} enterprise`)
                } */
                this.createTable({
                    tableName:user+'_registro_table',
                    valueNames:[
                        'id',
                        'id_plan',
                        'user_mail',
                        'Fecha',
                        'hora',
                        'entrada',
                        'salida',
                        'Ausencia',
                        'AusenciaConfirmada',
                        'Vacaciones',
                        'VacacionesConfirmadas'
                    ],
                    valueTypes:[
                        'SERIAL',
                        'Int',
                        'varchar(30)',
                        'varchar(30)',
                        'varchar(30)',
                        'Boolean',
                        'Boolean',
                        'Boolean',
                        'Boolean',
                        'Boolean',
                        'Boolean'
                    ],
                    pk:'id,id_plan',
                    fk:{
                        create:true,
                        referencedValues:['id'],
                        tableReferences:[empresa+'_plan_table'],
                        valueNames:['id_plan']
                    }
                }) 
                this.executeQuery().then(res=>{
                    resolve(this.get_result(res,'count'))
                }).catch(err=>{
                    reject(err)
                })
    
            /* }).catch(err=>{
                reject(err)
            }) */
       })
    }
    dropRegistroTable(user:any,){
        return new Promise((resolve,reject)=>{
            this.selectBy({
                select:{column:'*',from:user+'_registro_table'},
                conditionals_create:false,
                where_create:false,
            }).then((res:any[])=>{
                this.dropTable(user+'_registro_table')
                this.executeQuery().then(res=>{
                    resolve(this.get_result(res,'count'))
                }).catch(err=>{
                    reject(err)
                })
    
            }).catch(err=>{
                reject(err)
            })
       })
    }
    insertRegistro(data:registro,user:string){
        return new Promise((resolve,reject)=>{
            this.insert(user+'_registro_table')
            this.colums_insert_into(`id_plan,
                                    user_mail,
                                    Fecha,
                                    hora,
                                    entrada,
                                    salida,
                                    Ausencia,
                                    AusenciaConfirmada,
                                    Vacaciones,
                                    VacacionesConfirmadas`)
            this.values_insert_into(`${data.id_plan},
                                    '${user}',
                                    '${data.Fecha}',
                                    '${data.hora}',
                                    ${data.entrada},
                                    ${data.salida},
                                    ${data.Ausencia},
                                    ${data.AusenciaConfirmada},
                                    ${data.Vacaciones},
                                    ${data.VacacionesConfirmadas}`)
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    deleteRegistro(user:string,registro_id:number){
        return new Promise((resolve,reject)=>{
            this.delete(user+'_registro_table')
            this.where({column:'id',value:registro_id,condition:'=',conditionalnumber:'1',previous:'DELETE'})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res,'count'))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    updateRegistro(data:update){
        return new Promise((resolve,reject)=>{
            console.log(data.more_arguments);
            
            this.update(data.table+'_registro_table')
            this.update_set({column:data.arguments.column,
                            argument:data.arguments.argument,previous:'UPDATE'})
            if(data.moreArgumentsBool){
                data.more_arguments.forEach(value => {
                    this.more_update_set(({column:value.column,argument:value.argument,previous:'UPDATE'}))
                });
            }
            this.where({column:data.where.column,condition:data.where.condition,
                        conditionalnumber:'1',value:data.where.value,
                        previous:'UPDATE'})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res,'count'))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    selectWhereFromALlTables(sufix:string,where:any,empresa_name:string){
        return new Promise((resolve,reject)=>{
            this.getAllTablesWhere(sufix,empresa_name+"_user_table")
            this.executeQuery().then(res=>{
                let tables = this.get_result(res)
                for (let index = 0; index < tables.length; index++) {
                    this.select({columns:"*",table:tables[index].table_name})
                    if(index !== tables.length-1){
                        this.UNION()
                    } 
                }
                 
                this.executeQuery().then(res=>{
                    resolve(this.get_result(res));
                }).catch(err=>{
                    reject(err);
                    
                })
            }).catch(err=>{
                reject(err);
            })
        })
    }

}

export default RegistroModel