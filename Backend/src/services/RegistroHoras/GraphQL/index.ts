import {resolvers as registro_resolver} from "./resolvers/registro.resolver"
import {typeDefs as registro_typedef} from "./typedefs/registro.typedef"

export {
    registro_resolver,registro_typedef
}