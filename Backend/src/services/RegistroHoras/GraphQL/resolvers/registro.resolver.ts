import {CreateRegistroTable,selectAllRegistros,DropRegistroTable,insertRegistro,deleteRegistro,updateRegistro,
  selectRegistroWithWHERE,selectPlanFromALlTablesOfAnEmpresa} from "../../controllers/registro.controller"

const credentials = (user)=>{
  if(user.user_type!=="Jefe" && user.user_type!=="Admin" ){
    return {
      message:"You dont have credentials to do this",
      succes:false
    }
  }else{
    return "true"
  }
}
const resolvers = {
    Query: {
      selectAllRegistros(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        console.log(isAble);
        if(isAble=="true"){
          return selectAllRegistros(args.user,args.empresa_name)
        }else{
          return isAble
        }
          
      },
      selectRegistrosByRegistroID(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        if(isAble=="true"){
          return selectRegistroWithWHERE(args.user,args.empresa_name,{column:"id", value:args.registro_id})
        }else{
          return isAble
        }
        
      },
      selectPlanFromALlTablesOfAnEmpresa(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        if(isAble=="true"){
           return selectPlanFromALlTablesOfAnEmpresa(args.empresa_name)
        }else{
          return isAble
        }
       
      },
      selectRegistrosByFecha(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        if(isAble=="true"){
          return selectRegistroWithWHERE(args.user,args.empresa_name,{column:"fecha", value:args.fecha})
        }else{
          return isAble
        }
        
      },
    },
    Mutation: {
      createRegistroTable(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        if(isAble=="true"){
          return CreateRegistroTable({empresa:args.empresa,user:args.user})
        }else{
          return isAble
        }
        
      },
      DropRegistroTable(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        if(isAble=="true"){
          return DropRegistroTable({user:args.user})
        }else{
          return isAble
        }
        
      },
      insertRegistro(_obj:any, args:any, _context:any, _info:any){
          return insertRegistro(args.insert_input,args.user)
      },
      deleteRegistro(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        if(isAble=="true"){
          return deleteRegistro(args.user,args.registro_id)
        }else{
          return isAble
        }
        
      },
      updateRegistro(_obj:any, args:any, _context:any, _info:any){
        let isAble = credentials(_context)
        if(isAble=="true"){
          let info = args.update_input
          return updateRegistro({argument:info.argument,column:info.column,
                              table:info.table,where_value:info.where_value.int,
                              column_type:info.column_type})
        }else{
          return isAble
        }
      },
    }, 
    Registro:{
      plan(review:any) {
        console.log(review.empresa_name);  
        return { __typename: "Plan", id:review.id_plan,empresa_name:review.empresa_name};
      },
      admin(review:any) {
        /* console.log(review);   */
        return { __typename: "User", id:review.id_admin, empresa_name:review.empresa_name};
      } 
    }
  };
  
  
  
  export {
      resolvers
  }