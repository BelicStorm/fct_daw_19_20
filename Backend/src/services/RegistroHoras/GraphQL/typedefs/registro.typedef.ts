import { gql } from "apollo-server-express";
import registroInputs from "./registro.inputs"
import registroQuery from "./registro.query";
import types from "./registro.types"
import mutations from "./registro.mutation"



const typeDefs = gql`${registroInputs}${registroQuery}${types}${mutations}`;

export {
    typeDefs
}