const registroQuery = `
type Query{
    selectAllRegistros(user:String!,empresa_name:String!):[Registro!],
    selectRegistrosByAdminID(id_admin:Int!,empresa_name:String!):[Registro],
    selectRegistrosByFecha(user:String!,empresa_name:String!,fecha:String!):[Registro],
    selectRegistrosByRegistroID(user:String!,empresa_name:String!,registro_id:Int):[Registro],
    selectPlanFromALlTablesOfAnEmpresa(empresa_name:String!):[Registro]
}

`
export default registroQuery