const types = `
    type Registro_auth_response {
        succes:Boolean,
        message:String,
        token:String
    }

    type Registro @key(fields:"id, id_plan"){
        id:Int,
        empresa_name:String,
        id_plan:Int!,
        user_mail:String!,
        plan:Plan @provides(fields: "empresa_name"),
        fecha:String!,
        hora:String!
        entrada:Boolean!,
        salida:Boolean!,
        ausencia:Boolean!,
        ausenciaconfirmada:Boolean,
        vacaciones:Boolean!,
        vacacionesconfirmadas:Boolean
    },
    extend type Plan @key(fields: "id,empresa_name") {
        id: ID @external,
        empresa_name:String @external
    }
`

export default types

