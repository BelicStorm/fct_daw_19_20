const mutations = `

type Mutation {
    createRegistroTable(empresa:String!,user:String!):Registro_auth_response,
    DropRegistroTable(user:String!):Registro_auth_response,
    insertRegistro(insert_input:insert_input!,user:String!):Registro_auth_response,
    deleteRegistro(user:String!,registro_id:Int!):Registro_auth_response,
    updateRegistro(update_input:update_input!):Registro_auth_response,
}

`

export default mutations