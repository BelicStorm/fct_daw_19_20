const registroInputs = `
input RegIntOrString {
    int: Int 
    String: String
  }
input update_input{
  where_value:RegIntOrString!,
  table:String,
  column:[String],
  argument:[String],
  column_type:[String]
}
input insert_input{
    id:Int,
    id_plan:Int!,
    Fecha:String!,
    hora:String!
    entrada:Boolean!,
    salida:Boolean!,
    Ausencia:Boolean!,
    AusenciaConfirmada:Boolean!,
    Vacaciones:Boolean!,
    VacacionesConfirmadas:Boolean!
}

`
export default registroInputs