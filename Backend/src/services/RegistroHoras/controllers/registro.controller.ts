
import RegistroModel from "../models/registro.model"
import {auth_result_message, registro, update} from "../../../interfaces/intefarces"


function colum_parse(column,type):any{
    switch(type){
        case "boolean":
            return (column.toUpperCase() === 'TRUE')
        case "int":
            return parseInt(column)
        default:
            return column
    }
}
function addEmpresaName(empresa_name:string,registros:any){
    return new Promise((resolve,reject)=>{
        let new_plan = []
        registros.forEach(element => {
            new_plan.push({...element, empresa_name:empresa_name})
        });
        resolve(new_plan)
        
    })
}

function CreateRegistroTable(data:{empresa:string,user:any}):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel()
        Registro.createRegistroTable(data.empresa,data.user).then(res=>{
            resolve({
                message:`${data.user} Registro Table Created Successfully`,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}
function DropRegistroTable(data:{user:any}):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel()
        Registro.dropRegistroTable(data.user).then(res=>{
            resolve({
                message:`${data.user} Registro Table Deleted Successfully`,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}
function insertRegistro(data:registro,user:string):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel
        Registro.insertRegistro(data,user).then(res=>{
            resolve({
                message:`Registro Added Successfully`,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}
function deleteRegistro(user,registro_id):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel
        Registro.deleteRegistro(user,registro_id).then(res=>{
            resolve({
                message:`Registro Deleted Successfully`,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}
function updateRegistro(data:{column_type:string[],where_value:any,table:string,column:string[],argument:string[]}):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        const formated_data = ()=>{
            return new Promise((resolve,reject)=>{
                let moreArguments = data.argument.length > 1 ? true : false
                let formated_data:any = {
                    table:data.table,
                    arguments:{
                        argument:colum_parse(data.argument[0],data.column_type[0]),
                        column:data.column[0],
                    },
                    moreArgumentsBool:moreArguments,
                    where:{
                        column:"id",
                        condition:"=",value:data.where_value
                    },
                }
                if(moreArguments){
                    let more_arguments=[]
                    for (let index = 1; index <= data.argument.length-1; index++) {
                        console.log(data.column[index]);
                        
                        more_arguments.push(
                            {
                                argument:colum_parse(data.argument[index],data.column_type[index]),
                                column:data.column[index],
                            }
                        )
                        
                    }
                    formated_data = {
                        ...formated_data,more_arguments:more_arguments
                    }
                    console.log(formated_data.more_arguments);
                }
                resolve(formated_data)
            })
        }
        formated_data().then((data:update)=>{
            let Registro = new RegistroModel       
            Registro.updateRegistro(data).then(res=>{
                resolve({
                    message:`Registro updated Successfully, affected rows ${res}`,
                    succes:true
                })
            }).catch(err=>{
                reject({
                    message:err,
                    succes:false
                })
            })
        })
    })
}
function selectAllRegistros(userMail:string,empresa_name:string){
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel
        Registro.selectBy({
            select:{column:"*",from:userMail+"_registro_table"},
            conditionals_create:false,
            where_create:false
        }).then(res=>{
            addEmpresaName(empresa_name,res).then(res=>{
                resolve(res)
            })
        }).catch(err=>{
            resolve(err)
        })
    })
}

function selectRegistroWithWHERE(userMail:string,empresa_name:string,where:any){
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel
        Registro.selectBy({
            select:{column:"*",from:userMail+"_registro_table"},
            conditionals_create:false,
            where_create:true,
            where:{column:where.column,condition:"=",value:where.value,previous:"SELECT",conditionalnumber:"1"}
        }).then(res=>{
            addEmpresaName(empresa_name,res).then(res=>{
                resolve(res)
            })
        }).catch(err=>{
            resolve(err)
        })
    })
}

function selectPlanFromALlTablesOfAnEmpresa(empresa_name:string){
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel
        Registro.selectWhereFromALlTables("_registro_table",
                                        {create:true,column:"id_plan"},
                                        empresa_name).then(res=>{
            addEmpresaName(empresa_name,res).then(res=>{
                resolve(res)
            })
        }).catch(err=>{
            reject(err)
        })
    })
}
/* function selectWhereAdminIdFromALlTables(admin_id:number,empresa_name:string){
    return new Promise((resolve,reject)=>{
        let Registro = new RegistroModel
        Registro.selectWhereFromALlTables("_registro_table",{cerate:true,column:"id_admin",value:admin_id}).then(res=>{
            addEmpresaName(empresa_name,res).then(res=>{
                resolve(res)
            })
        }).catch(err=>{
            reject(err)
        })
    })
} */



export {CreateRegistroTable,selectAllRegistros,DropRegistroTable,insertRegistro,deleteRegistro,updateRegistro,selectRegistroWithWHERE,
    selectPlanFromALlTablesOfAnEmpresa} 