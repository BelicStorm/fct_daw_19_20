import {select_all_users,deleteUser,createTable,dropTable,insertUser,updateUser,login,getUserById,getUserBymail} from "../../controllers/auth.controller"
const credentials = (user)=>{
  if(user.user_type!=="Jefe" && user.user_type!=="Admin" ){
    return {
      message:"You dont have credentials to do this",
      succes:false
    }
  }else{
    return "true"
  }
}
const resolvers = {
  Query: {
    SelectAllUsers(_obj:any, args:any, _context:any, _info:any){
      console.log(_context);
      
      let isAble = credentials(_context)
      if(isAble==="true"){
        return select_all_users(args.empresa)
      }else{
        return isAble
      }
   },
    SelectUserByMail(_obj:any, args:any, _context:any, _info:any){
    return getUserBymail(args.email,args.empresa)
   },
   login(_obj:any, args:any, _context:any, _info:any){
     return login({email:args.email,password:args.password},args.empresa)
   }
  },
  Mutation: {
    insertUser(_obj:any, args:any, _context:any, _info:any){
      console.log(_context);
        return insertUser({email:args.email,password:args.password,
                          nombre:args.nombre,id_empresa:args.id_empresa,
                          user_type:args.user_type,plan_id:args.plan_id},args.empresa)
    },
    createUserTable(_obj:any, args:any, _context:any, _info:any){
      return createTable(args.table_name)
    },
    dropUserTable(_obj:any, args:any, _context:any, _info:any){
      return dropTable(args.table_name)
    },
    deleteUser(_obj:any, args:any, _context:any, _info:any){
      return deleteUser({userMail:args.userMail,table:args.empresa})
    },
    updateUser(_obj:any, args:any, _context:any, _info:any){
      return updateUser(args)
    }
  },
  User: {
    empresa(review:any) {
      console.log(review);  
      return { __typename: "Empresa", id:review.id_empresa};
    },
    plan(review:any) {
      console.log("From Plan User"); 
      console.log(review);  
      return { __typename: "Plan", id:review.plan_id,empresa_name:review.empresa_name};
    },
    __resolveReference: reference=>{ 
      console.log("from user reRe:");
      console.log(reference);
      return getUserById(reference.id,reference.empresa_name)
    }
  },
};



export {
    resolvers
}