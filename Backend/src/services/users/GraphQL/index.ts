import {resolvers as user_resolver} from "./resolvers/user.resolver"
import {typeDefs as user_typedef} from "./typedefs/user.typedef"

export {
    user_resolver,user_typedef
}