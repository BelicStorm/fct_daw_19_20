import { gql } from "apollo-server-express";


const typeDefs = gql`

type Query{
  SelectAllUsers(empresa:String!):[User]!,
  login(email:String!,password:String!,empresa:String!):auth_response!,
  SelectUserByMail(email:String!,empresa:String!):[User]!,
}
type Mutation{
  insertUser(email:String,password:String,nombre:String,id_empresa:Int, empresa:String!,user_type:String!,plan_id:Int!):auth_response,
  createUserTable(table_name:String!):auth_response!,
  dropUserTable(table_name:String!):auth_response!,
  deleteUser(userMail:String!,empresa:String!):auth_response!,
  updateUser(  
    user_id:Int!,
    table:String!,
    column:[String!]!,
    argument:[String!]!
    ):auth_response
}
type User @key(fields: "id") {
  id: ID!,     
  email: String,      
  id_empresa: Int!,
  plan:Plan @provides(fields: "empresa_name"),
  plan_id:Int!
  empresa_name: String,
  nombre: String,      
  contrasena: String,
  user_type:String!
  empresa:Empresa
}
type auth_response {
  succes:Boolean,
  message:String,
  token:String,
  user_type:String,
  plan_id:Int
}
extend type Empresa @key(fields: "id") {
  id: ID! @external,
}
extend type Plan @key(fields: "id,empresa_name") {
  id: ID @external,
  empresa_name:String @external
}

`;

export {
    typeDefs
}