import Model from "../../../model/database"
import {update} from "../../../interfaces/intefarces"
import Utils from "../../../utils/utils"

class UserModel extends Model{

    select_all_users(table_name:string){
        return new Promise((resolve,reject)=>{
            this.select({
                columns:"*",
                table:table_name+"_user_table"
            })
            this.executeQuery().then(res=>{
                let result= this.get_result(res)
                resolve(result)
            }).catch(err=>{
                reject(err);
            })
        })
    }
    not_exists_in_user_table(data:any){
        return new Promise((resolve,Reject)=>{
            this.select({columns:data.select.columns,table:data.select.table})
            this.where({column:data.where.column,condition:data.where.condition,conditionalnumber:"1",value:data.where.value,previous:"SELECT"})
            this.executeQuery().then(_res=>{
                let result = this.get_result(_res)
                /* console.log(result.length); */
                if(result.length==0){
                    resolve(true)
                }else{
                    Reject("The user allready exists")
                }
            }).catch(err=>{
                Reject('Error acquiring client'+ err)
            })
        })  
    }
    insert_user(data:any,empresa:string){
        return new Promise((resolve,reject)=>{
            this.ifExists(empresa+"_user_table")
            this.executeQuery().then(_res=>{
            this.not_exists_in_user_table(
                    {
                        select:{columns:"*",table:empresa+"_user_table"},
                        where:{column:"email",condition:"=",value:data.email}
                    }
                ).then(res=>{
                    this.insert(empresa+"_user_table")
                    this.colums_insert_into("email,id_empresa,nombre,contrasena,user_type,plan_id")
                    let pass = Utils.encrypt(data.password)          
                    console.log(pass);
                    
                    this.values_insert_into(`'${data.email}',${data.id_empresa},'${data.nombre}','${pass.encryptedData}','${data.user_type}',${data.plan_id}`)
                    this.executeQuery().then(res=>{
                        console.log(this.get_result(res,"count"));
                        resolve(this.get_result(res,"count"))
                    }).catch(err=>{
                        reject('Error acquiring client'+ err)
                    })
                }).catch(err=>{
                    reject('Error acquiring client'+ err)
                })
            }).catch(err=>{
                if (err) {
                    reject('Error acquiring client'+ err)
                }
            })
        })
    }
    createUserTable(table_name:string){
        return new Promise((resolve,reject)=>{
            this.createTable({
                    tableName:table_name+"_user_table",
                    valueNames:["id","email","id_empresa","nombre","contrasena","user_type","plan_id"],
                    valueTypes:["SERIAL","varchar(30)","Int","varchar(30)","TEXT","varchar(30)","Int"],
                    pk:"id",
                    fk:{
                        create:true,
                        referencedValues:["id","id","id"],
                        tableReferences:["empresa",table_name+"_plan_table"],
                        valueNames:["id_empresa","plan_id"]
                    }
            })
            this.executeQuery().then(res=>{
                console.log(res);
                let result = this.get_result(res,"count")
                resolve(result);
            }).catch(err=>{
                reject(err)
            })
        })
        
    }
    dropUserTable(table:string){
        return new Promise((resolve,reject)=>{
            this.dropTable(table+"_user_table")
            this.executeQuery().then(res=>{
                resolve(this.get_result(res,"count"))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    deleteUsers(data:any){
        console.log(data);
        
        return new Promise((resolve,reject)=>{
            this.delete(data.table+"_user_table")
            this.where({column:"email",condition:"=",conditionalnumber:"1",value:data.userMail,previous:"DELETE"})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res,"count"))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    updateUser(data:update){
        return new Promise((resolve,reject)=>{
            this.update(data.table+"_user_table")
            this.update_set({column:data.arguments.column,
                            argument:data.arguments.argument,previous:"UPDATE"})
            if(data.moreArgumentsBool){
                data.more_arguments.forEach(value => {
                    this.more_update_set(({column:value.column,argument:value.argument,previous:"UPDATE"}))
                });
            }
            this.where({column:data.where.column,condition:data.where.condition,
                        conditionalnumber:"1",value:data.where.value,
                        previous:"UPDATE"})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    selectById(data:any){
        return new Promise((resolve,reject)=>{
            this.select({table:data.table+"_user_table",columns:"*"})
            this.where({column:"id",condition:"=",conditionalnumber:"1",value:data.value,previous:"SELECT"})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    selectByMail(data:any){
        console.log(data);
        
        return new Promise((resolve,reject)=>{
            this.select({table:data.table+"_user_table",columns:"*"})
            this.where({previous:"SELECT",column:"email",condition:"=",conditionalnumber:"1",value:data.email})
            this.executeQuery().then(res=>{
                console.log(this.get_result(res));
                
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    login(data:any,table_name:string){
        return new Promise((resolve,reject)=>{
            this.select({table:table_name+"_user_table",columns:"*"})
            this.where({previous:"SELECT",column:"email",condition:"=",conditionalnumber:"1",value:data.email})
            this.executeQuery().then(res=>{
                let result = this.get_result(res)[0]
                if (Utils.compare_cifrate(data.password,result.contrasena)) {
                    resolve({
                        token:Utils.jwtSign({email:result.email,user_type:result.user_type}),
                        user_type:result.user_type, plan_id: result.plan_id
                    })
                }
                reject("There are somthing wrong in your acces data")
            }).catch(err=>{
                reject(err)
            })
        })
    }

}

export default UserModel