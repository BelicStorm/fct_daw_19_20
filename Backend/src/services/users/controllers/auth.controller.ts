import {update} from "../../../interfaces/intefarces"
import UserModel from "../models/user.model"

function addEmpresaName(empresa_name:string,results:any){
    return new Promise((resolve,reject)=>{
        let with_empresa = []
        results.forEach(element => {
            with_empresa.push({...element, empresa_name:empresa_name})
        });
        resolve(with_empresa)
        
    })
}
const select_all_users =(table_name:string)=>{
    return new Promise((resolve,reject)=>{
        let user = new UserModel()
        user.select_all_users(table_name).then(res=>{
            addEmpresaName(table_name,res).then(res_with_empresa=>{
                resolve(res_with_empresa)
            })
        }).catch(err=>{
            reject(err);
        })
    })
}
const getUserBymail = (email:string,table:string)=>{
    return new Promise((resolve,reject)=>{
        let user = new UserModel; 
        user.selectByMail({table:table, email:email}).then(res=>{  
            resolve(res)
        }).catch(err=>{
            resolve(err)
        })
    })
}
const deleteUser = (data:any)=>{
    return new Promise((resolve,reject)=>{
        let user = new UserModel;
        user.deleteUsers(data).then(res=>{
            resolve({
                message:`User with mail: ${data.userMail} deleted`,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })

    })
}
const insertUser = (data:any,empresa:string) =>{
  return new Promise((resolve,reject)=>{
    let user = new UserModel() 
    user.insert_user(data,empresa).then(res=>{
        console.log(res);
        resolve({
            message:"Affected rows:"+res+" User added succesfully",
            succes:true
        })
    }).catch(err=>{
        console.log("error in controller"+err);
        reject({
            message:err,
            succes:false
        })
    })
  })

}
const createTable = (table_name:string) =>{
   return new Promise((resolve,reject)=>{
        let user = new UserModel() 
        user.createUserTable(table_name).then(res=>{
            resolve(
                {
                    message:"Affected rows:"+res+" Table Created succesfully",
                    succes:true
                }
            )
        }).catch(err=>{
            reject(err)
        })
   })

}
const dropTable = (table:string) =>{
    return new Promise((resolve,reject)=>{
        let user = new UserModel() 
        user.dropUserTable(table).then(res=>{
            resolve(
                {
                    message:"Affected rows:"+res+" Table Deleted succesfully",
                    succes:true
                }
            )
        }).catch(err=>{
            reject(
                {
                    message:err,
                    succes:false
                }
            )
        })
    })

}
const updateUser = (data:any)=>{
    return new Promise((resolve,reject)=>{
        let user = new UserModel; 
        let moreArguments = data.argument.length > 1 ? true : false
        let formated_data:update = {
            table:data.table,
            arguments:{
                argument:data.argument[0],
                column:data.column[0],
            },
            moreArgumentsBool:moreArguments,
            where:{
                column:"id",condition:"=",value:data.user_id
            },
        }
        if(moreArguments){
            let more_arguments:any=[]
            for (let index = 1; index <= data.argument.length-1; index++) {
                more_arguments.push(
                    {
                        argument:data.argument[index],
                        column:data.column[index],
                    }
                )
                
            }
            formated_data = {
                ...formated_data,more_arguments:more_arguments
            }
        }
        user.updateUser(formated_data).then(res=>{
            resolve({
                message:`${formated_data.table} updated. Rows afected: ${res}`,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })

    })
}
const getUserById = (id:string,table:string)=>{
    return new Promise((resolve,reject)=>{
        let user = new UserModel; 
        user.selectById({table:table, value:id}).then(res=>{  
            resolve(res[0])
        }).catch(err=>{
            resolve(err)
        })
    })
}
const login = (data:any,table:string) =>{
    return new Promise((resolve,reject)=>{
        let user = new UserModel() 
        user.login(data,table).then((res:any)=>{
            resolve(
                {
                    message:"Loged succesfully",
                    succes:true,
                    token:res.token,
                    user_type:res.user_type,
                    plan_id:res.plan_id
                }
            )
        }).catch(err=>{
            reject(
                {
                    message:err,
                    succes:false
                }
            )
        })
    })

}


export {
    select_all_users,dropTable,createTable,insertUser,deleteUser,
    updateUser,login,getUserById,getUserBymail
}