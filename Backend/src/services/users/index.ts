import "reflect-metadata"

import FederatedService from "../service.template"
import {user_resolver,user_typedef} from "./GraphQL"


//Creamos una instancia de la clase de Servicios Federados a la cual le pasamos los resolvers, los typedefs y si necesita o no una conexión con TypeOrm
export async function listen(port: number) {
    
    const service = new FederatedService({resolvers:user_resolver,typeDefs:user_typedef,typeORMConfig:false})
     
    return await service.listen(port)
      
}