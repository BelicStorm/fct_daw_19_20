import "reflect-metadata"

import FederatedService from "../service.template"
import {plan_resolver,plan_typedef} from "./GraphQL"

//Creamos una instancia de la clase de Servicios Federados a la cual le pasamos los resolvers, los typedefs y si necesita o no una conexión con TypeOrm
export async function listen(port: number) {
    
    const service = new FederatedService({resolvers:plan_resolver,typeDefs:plan_typedef,typeORMConfig:false})
     
    return await service.listen(port)
      
}