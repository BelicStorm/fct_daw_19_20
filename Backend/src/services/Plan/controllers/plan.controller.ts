
import PlanModel from "../models/plan.model"
import {auth_result_message, update} from "../../../interfaces/intefarces"


function CreatePlanTable(empresa_name:string):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Plan = new PlanModel()
        Plan.createPlanTable(empresa_name).then(res=>{
            resolve({
                message:`${empresa_name} Plan Table Created Successfully`,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}

function createPlan(data:any,empresa_name:string):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let plan = new PlanModel()
        plan.insertPlan({plan_name:data.plan_name,
                         vacaciones:data.vacaciones,horas_dia:data.horas_dia,
                        hora_entrada:data.hora_entrada,hora_salida:data.hora_salida},empresa_name)
        .then(res=>{
            resolve({
                message:"Plan Insert correctly",
                succes:true
            })
        }).catch(err=>{
            console.log(err);
            reject({
                message:err,
                succes:false
            })
        })
    })

}
/**
* @param {object} 
{
    empresa:String,
    option:int (1 = id, 2 = plan_name),
    value:string||int
}

*/
function deletePlanBy(data:any):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Plan = new PlanModel()
        Plan.deletePlan({table_name:data.empresa,
                         column:data.option == 1 ? "id" : "plan_name",
                         where_value:data.value}).then(res=>{
            resolve({
                message:"Deleted successfully. Affected Rows:"+res,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}

function updatePlanBy(data:{option:1|2,where_value:any,table:string,column:string[],argument:string[]}):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Plan = new PlanModel()
        let moreArguments = data.argument.length > 1 ? true : false
        let formated_data:update = {
            table:data.table,
            arguments:{
                argument:data.argument[0],
                column:data.column[0],
            },
            moreArgumentsBool:moreArguments,
            where:{
                column:data.option == 1 ? "id" : "plan_name",
                condition:"=",value:data.where_value
            },
        }
        if(moreArguments){
            let more_arguments:any=[]
            for (let index = 1; index <= data.argument.length-1; index++) {
                more_arguments.push(
                    {
                        argument:data.argument[index],
                        column:data.column[index],
                    }
                )
                
            }
            formated_data = {
                ...formated_data,more_arguments:more_arguments
            }
        }
        Plan.updatePlan(formated_data).then(res=>{
            resolve({
                message:"Updated successfully. Affected Rows:"+res,
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}

function dropPlanTable(empresa_name:string):Promise<auth_result_message>{
    return new Promise((resolve,reject)=>{
        let Plan = new PlanModel()
        Plan.dropPlanTable(empresa_name).then(res=>{
            resolve({
                message:"Plan Table deleted correctly",
                succes:true
            })
        }).catch(err=>{
            reject({
                message:err,
                succes:false
            })
        })
    })
}

function addEmpresaName(empresa_name:string,plans:any){
    return new Promise((resolve,reject)=>{
        let new_plan = []
        plans.forEach(element => {
            new_plan.push({...element, empresa_name:empresa_name})
        });
        resolve(new_plan)
        
    })
}

function getAllPlans(empresa_name:string):Promise<any>{
    return new Promise((resolve,reject)=>{
        let Plan = new PlanModel()
        Plan.getAllPlan(empresa_name).then(res=>{     
            addEmpresaName(empresa_name,res).then(res=>{
                resolve(res)
            })
        }).catch(err=>{
            reject(err)
        })
    })
}

function getPlanById(empresa_name:string,plan_id:number){
    return new Promise((resolve,reject)=>{
        console.log(empresa_name);
        
        let Plan = new PlanModel()
        Plan.selectBy({
            conditionals_create:false,where_create:true,
            select:{column:"*",from:empresa_name+"_plan_table"},
            where:{column:"id",value:plan_id,condition:"=",conditionalnumber:"1",previous:"SELECT"}
        }).then(res=>{
            addEmpresaName(empresa_name,res).then(res=>{
                console.log(res);
                resolve(res[0])
            })
        }).catch(err=>{
            resolve(err)
        })
    })
}

export {createPlan,CreatePlanTable,dropPlanTable,getAllPlans,deletePlanBy,updatePlanBy,getPlanById} 