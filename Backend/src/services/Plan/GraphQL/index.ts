import {resolvers as plan_resolver} from "./resolvers/plan.resolver"
import {typeDefs as plan_typedef} from "./typedefs/plan.typedef"

export {
    plan_resolver,plan_typedef
}