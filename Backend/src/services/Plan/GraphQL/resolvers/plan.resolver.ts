import {createPlan,CreatePlanTable,dropPlanTable,getAllPlans,deletePlanBy,updatePlanBy,getPlanById} from "../../controllers/plan.controller"
const resolvers = {
    Query: {
      getAllPlans(_obj:any, args:any, _context:any, _info:any){
        return getAllPlans(args.empresa)
      },
      getPlanById(_obj:any, args:any, _context:any, _info:any){
        return getPlanById(args.empresa, args.plan_id)
      },
    },
    Mutation: {
      createPlanTable(_obj:any, args:any, _context:any, _info:any){
          return CreatePlanTable(args.empresa)
      },
      createPlan(_obj:any, args:any, _context:any, _info:any){
        
          return createPlan(args.plan,args.empresa)
       
          
      },
      dropPlanTable(_obj:any, args:any, _context:any, _info:any){
        if(_context.user_type==="Jefe"){
          return dropPlanTable(args.empresa)
        }else{
          return {
            message:"You dont have credentials to do this",
            succes:false
          }
        }
        
      },
      deletePlanById(_obj:any, args:any, _context:any, _info:any){
        if(_context.user_type==="Jefe"){
          return deletePlanBy({empresa:args.empresa,option:1,value:args.value.int})
        }else{
          return {
            message:"You dont have credentials to do this",
            succes:false
          }
        }
        
      },
      deletePlanByPlanName(_obj:any, args:any, _context:any, _info:any){
        if(_context.user_type==="Jefe"){
          return deletePlanBy({empresa:args.empresa,option:2,value:args.value.string})
        }else{
          return {
            message:"You dont have credentials to do this",
            succes:false
          }
        }
      },
      updatePlanById(_obj:any, args:any, _context:any, _info:any){
        if(_context.user_type==="Jefe"){
          let info = args.input
          return updatePlanBy({argument:info.argument,column:info.column,option:1,table:info.table,where_value:info.where_value.int})
        }else{
          return {
            message:"You dont have credentials to do this",
            succes:false
          }
        }
       
      },
      updatePlanByPlanName(_obj:any, args:any, _context:any, _info:any){
        if(_context.user_type==="Jefe"){
          let info = args.input
          return updatePlanBy({argument:info.argument,column:info.column,option:2,table:info.table,where_value:info.where_value.string})
        }else{
          return {
            message:"You dont have credentials to do this",
            succes:false
          }
        }
      },
     },
    Plan: {
      /* creator(review:any) {
        console.log(review);  
        return { __typename: "User", id:review.id_user, empresa_name:review.empresa_name};
      }, */
      __resolveReference: reference=>{ 
        console.log("from Plan reRe:");
        console.log(reference);
        return getPlanById(reference.empresa_name,reference.id)
      }
    },
     
  };
  
  
  
  export {
      resolvers
  }