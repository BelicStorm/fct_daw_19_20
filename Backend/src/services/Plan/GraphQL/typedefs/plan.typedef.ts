import { gql } from "apollo-server-express";


const typeDefs = gql`

input IntOrString {
  int: Int
  string: String
}
input InputPlan {
  plan_name:String!,
  horas_dia:Int!,
  vacaciones:Int!,
  hora_entrada:String!,
  hora_salida:String!
}
input Update{
  where_value:IntOrString,
  table:[String],
  column:[String],
  argument:[String]
}

type Query{
  getAllPlans(empresa:String!):[Plan!],
  getPlanById(empresa:String!,plan_id:Int!):Plan!,
}

type Mutation{
  createPlanTable(empresa:String!):result_response,
  createPlan(plan:InputPlan!,empresa:String!):result_response,
  dropPlanTable(empresa:String!):result_response,
  updatePlanById(input:Update):result_response,
  updatePlanByPlanName(input:Update):result_response,
  deletePlanById(empresa:String,value:IntOrString):result_response,
  deletePlanByPlanName(empresa:String,value:IntOrString):result_response,
}
type result_response {
  succes:Boolean!,
  message:String!,
  token:String,
}
type Plan @key(fields: "id"){
  id:ID,
  empresa_name:String,
  plan_name:String! ,
  horas_dia:Int!,
  vacaciones:Int!,
  hora_entrada:String!,
  hora_salida:String!
}
`;

export {
    typeDefs
}