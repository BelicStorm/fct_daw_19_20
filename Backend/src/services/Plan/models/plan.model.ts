import Model from "../../../model/database"
import {select,where_and_or,update, select_where_and_or} from "../../../interfaces/intefarces"

class PlanModel extends Model{
    selectBy(data:select_where_and_or){
        return new Promise((resolve,reject)=>{
            this.select({columns:data.select.column,table:data.select.from+""})
            if (data.where_create) {
                this.where({
                    column:data.where.column,condition:data.where.condition,conditionalnumber:'1',
                    value:data.where.value,previous:'SELECT'
                })
            }
            if(data.conditionals_create){
                for (let index = 0; index < data.conditionals.option.length; index++) {
                    if(data.conditionals.option[index]==='AND'){
                        this.and({
                            column:data.conditionals.columns[index],condition:data.conditionals.conditions[index],
                            conditionalnumber:`${index+1}`,value:data.conditionals.values[index],previous:'WHERE'
                        })
                    }else{
                        this.or({
                            column:data.conditionals.columns[index],condition:data.conditionals.conditions[index],
                            conditionalnumber:`${index+1}`,value:data.conditionals.values[index],previous:'WHERE'
                        })
                    }
                }
            }
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }

    createPlanTable(user_table:string){
       return new Promise((resolve,reject)=>{
            this.createTable({
                tableName:user_table+"_plan_table",
                valueNames:["id","plan_name","horas_dia","vacaciones","hora_entrada","hora_salida"],
                valueTypes:["SERIAL","varchar(30) UNIQUE","Int","Int","varchar(30)","varchar(30)"],
                pk:"id",
                fk:{
                    create:false,
                }
            })
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
       })
    }

    dropPlanTable(empresa_name:string){
        return new Promise((resolve,reject)=>{
            this.dropTable(empresa_name+"_plan_table")
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }

    insertPlan(data:any,table_name:string){
        return new Promise((resolve,reject)=>{
            console.log(table_name);
            
            this.getPlanWhere({table:table_name,columns:"*"},
                          {column:"plan_name",condition:"=",conditionalnumber:"1",
                          value:data.plan_name,previous:"SELECT"}).then(res=>{
                this.insert(table_name+"_plan_table")
                this.colums_insert_into(`plan_name,horas_dia,vacaciones,hora_entrada,hora_salida`)
                this.values_insert_into(`'${data.plan_name}',${data.horas_dia},${data.vacaciones},'${data.hora_entrada}','${data.hora_salida}'`)
                this.executeQuery().then(res=>{
                    resolve(this.get_result(res))
                }).catch(err=>{
                    reject(err)
                })
            
            }).catch(err=>{
                reject(err)
            })
        })
    }

    getPlanWhere(select:select,where:where_and_or){
        return new Promise((resolve,reject)=>{
            this.select({table:select.table+"_plan_table",columns:select.columns})
            this.where({column:where.column,condition:where.condition,
                        conditionalnumber:where.conditionalnumber,
                        value:where.value,previous:where.previous})
            this.executeQuery().then(res=>{
                console.log(this.get_result(res,"count"));
                
                resolve(this.get_result(res,"count"))
            }).catch(err=>{
                reject(err)
            })
        })
    }

    getAllPlan(empresa_name:string){
        return new Promise((resolve,reject)=>{
            this.select({columns:"*",table:empresa_name+"_plan_table"})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    deletePlan(data:{table_name:string,column:string,where_value:string}){
        return new Promise((resolve,reject)=>{
            this.delete(data.table_name+"_plan_table")
            this.where({previous:"DELETE",
                        column:data.column,condition:"=",
                        conditionalnumber:"1",value:data.where_value})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res,"count"))
            }).catch(err=>{
                reject(err)
            })
        })
    }
    updatePlan(data:update){
        return new Promise((resolve,reject)=>{
            this.update(data.table+"_plan_table")
            this.update_set({column:data.arguments.column,
                            argument:data.arguments.argument,previous:"UPDATE"})
            if(data.moreArgumentsBool){
                data.more_arguments.forEach(value => {
                    this.more_update_set(({column:value.column,argument:value.argument,previous:"UPDATE"}))
                });
            }
            this.where({column:data.where.column,condition:data.where.condition,
                        conditionalnumber:"1",value:data.where.value,
                        previous:"UPDATE"})
            this.executeQuery().then(res=>{
                resolve(this.get_result(res,"count"))
            }).catch(err=>{
                reject(err)
            })
        })
    }


}

export default PlanModel