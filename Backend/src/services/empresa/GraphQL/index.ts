import {resolvers} from "./resolvers/empresa.resolver"
import {typeDefs} from "./typedefs/empresa.typedef"

export {
    resolvers,typeDefs
}