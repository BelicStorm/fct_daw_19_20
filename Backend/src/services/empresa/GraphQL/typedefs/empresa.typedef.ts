import { gql } from "apollo-server-express";


const typeDefs = gql`

type Query {
  getAllEmpresas:[Empresa],
  getEmpresaById(id: Int!):Empresa!,
  getEmpresaByName(nombre:String!):Empresa!
}

type Mutation {
  registerNewEmpresa(nombre: String!,account_type: String!):response!,
  deleteEmpresaById(id: Int!):response!,
  updateEmpresaById(id: Int!,nombre: String,account_type: String):response!
}

type Empresa @key(fields: "id") {
  id: ID!
  nombre:String,
  account_type:String
}
type response {
  succes:Boolean!,
  message:String!,
  empresa:Empresa
}
`;

export {
    typeDefs
}