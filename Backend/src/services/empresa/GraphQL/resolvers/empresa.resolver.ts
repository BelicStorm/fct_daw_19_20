import {create,deleteById,get,getById,updateById,getByName} from "../../controllers/auth.controller"
import {empresa} from "../../../../interfaces/intefarces"

const resolvers = {
  Query: {
    getAllEmpresas(){
      return get()
    },
    getEmpresaById(_obj:any, args:any, _context:any, _info:any){
      return getById(args.id)
    },
    getEmpresaByName(_obj:any, args:any, _context:any, _info:any){
      return getByName(args.nombre)
    }
  },
  Mutation:{
    registerNewEmpresa(_parent:any, args:empresa){
      return create({nombre:args.nombre,account_type:args.account_type})
    },
    deleteEmpresaById(_parent:any, args:any){
      return deleteById(args.id)
    },
    updateEmpresaById(_parent:any, args:empresa){
      return updateById(args.id,{nombre:args.nombre,account_type:args.account_type})
    }
  },
  Empresa: {
    __resolveReference: referemce=>{ 
      console.log("from empresa reRe:");
      console.log(referemce);
      return getById(referemce.id)}
  }
};



export {
    resolvers
}