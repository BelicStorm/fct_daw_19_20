import {getRepository} from "typeorm"
import {Empresa} from "../../../model/TypeORM/entity/empresa.entity"

const get = async () =>{
    const  response = await getRepository(Empresa).find();
    return response
}
const getById = async (id:string) =>{
  const  response = await getRepository(Empresa).findOne(id);
  return response
}
const create = async (data:any) =>{
    try {
      const firstUser = await getRepository(Empresa)
                      .createQueryBuilder("empresa")
                      .where("empresa.nombre = :nombre", { nombre: data.nombre })
                      .getOne();
      if (firstUser === undefined) {
        const newData =  getRepository(Empresa).create(data);
        const response = await getRepository(Empresa).save(newData);
        return {
            succes:true,
            message:"Empresa Creada correctamente",
            empresa:response
        }
      } else {
        return {
            succes:false,
            message:"Empresa El nombre de esta empresa ya existe",
        }
      }          
    } catch (error) {
        return {
            succes:true,
            message:error
        }
    }
}
const deleteById = async (id:string) =>{
  try {
    await getRepository(Empresa).delete(id);
    return {
        succes:true,
        message:"Empresa Borrada stisfactoriamente",
    }
  } catch (error) {
    return {
        succes:true,
        message:error
    }
  }
}
const updateById = async (id:number,data:object) =>{
  const request =  await getRepository(Empresa).findOne(id);
  if(request){
     try {
        getRepository(Empresa).merge(request,data);
        const response = await getRepository(Empresa).save(request);
        return {
            succes:true,
            message:"Empresa Actualizada correctamente",
            empresa:response
        }
     } catch (error) {
        return {
            succes:false,
            message:"Fallo en la actualizacion",
        }
     }
  }else{
    return {
        succes:false,
        message:"La empresa no existe",
    }
  }
  
  
}
const getByName = async (name:string) =>{
  const firstUser = await getRepository(Empresa)
    .createQueryBuilder("empresa")
    .where("empresa.nombre = :nombre", { nombre: name })
    .getOne();
  return firstUser
}
export {
    get,getById,create,deleteById,updateById,getByName
}