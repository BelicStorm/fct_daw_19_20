import {Pool,PoolConfig, PoolClient, QueryResult} from "pg"
import dotenv from 'dotenv';

dotenv.config();


interface select{
    table:string,
    columns: string | string[]
}
interface where_and_or {
    column:string,
    condition:string,
    conditionalnumber:string,
    value:any,
    previous?:string
}
interface createTableProps{
    tableName:string,
    valueNames:string[],
    valueTypes:string[],
    fk:{
        create:boolean,
        valueNames?: string[] ,
        tableReferences?:string[],
        referencedValues?: string[]
    },
    pk:string
}
class Model {
    // Creamos las variables que van a contener toda la información con la configuración de la query o de la conexión con la base de datos
    private config:PoolConfig
    private pool:Pool
    private query:string ='';
    private values:string[] =[]
    constructor(){
        //Creamos la configuración que va a seguir cada una de las conexiones a base de datos que se realicen en los servicios que dependan de esta clase
        this.config = {
            user: process.env.POSTGRES_USER,
            host: "postgres",
            password: process.env.POSTGRES_PASSWORD,
            database: process.env.POSTGRES_DB,
            port:5432
        };
        //Guardamos la configuración resultante en nuestra variable para que los demás métodos puedan usarla
        this.pool = new Pool(this.config);
        this.pool.on('connect', () => {
          console.log('connected to the Database');
        });
    }
/* //El método que ejecuta las queries está formado por una promesa en la cual se ejecuta la conexión a postgre, usando la configuración 
previamente guardada. Tras esto y con la conexión realizada, realizamos la ejecución de la sentencia guardada en la variable Query. 
Ocasionalmente algunas queries requeriran values, estos se guardaran una variable de nombre homónimo. */
    protected executeQuery(){
       return new Promise((resolve,reject)=>{
        this.pool.connect( (err:Error,client:PoolClient,done)=>{
            if (err) {
                this.clearQuery()
                reject(err.stack) 
            }
           client.query(this.query, this.values || [], (query_error: Error, result: QueryResult<any>) => {
                done();
                if (query_error) {
                    this.clearQuery()
                    reject(query_error.stack) 
                }
                this.clearQuery()
                resolve(result)
            })
        })
       })
    }
    /* ///////////////////////// Estas sentencias genéricas las utilizaremos en el caso de ejecutar sentencias que se repitan con la misma 
    sintaxis como getAllTablesWhere, o para verificar la parte de la sentencia que queremos añadir cumple con la sintaxis de Postgre */
    protected getAllTablesWhere(table_sufix:string,table_name:string){
        
        this.query= `SELECT table_name FROM information_schema.tables  INNER JOIN ${table_name} ON LOWER(${table_name}.email) = SPLIT_PART(table_name,'_','1');`
    }
    /**
    * @params "{JOIN_type:String!(INNER,OUTER,LEFT,RIGTH),
    *           JOIN_to:String(table_name)
    *           JOIN_data:[{on_OR_condition:String(ON,OR,AND),detonant:String(example: tablename.column = "test")}]
    *          }"
    */
    protected JOIN(structure:any){
        switch(structure.JOIN_type){
            case "INNER":
                this.query +=" INNER JOIN " + structure.JOIN_to
                break
            case "OUTER":
                this.query +=" OUTER JOIN "
                break
            case "LEFT":
                this.query +=" LEFT JOIN "
                break
            case "RIGTH":
                this.query +=" RIGTH JOIN "
                break
        }
        structure.JOIN_data.forEach((value,index)=>{ 
            this.query += `${value.on_OR_condition} ${value.detonant}`
        })

    }
    protected UNION(){
        this.query+=" UNION "
    }
    private isPosibleToContinue(previousSentence:string){
        return this.query.includes(previousSentence)
    }
    /* Tras la ejecución de una sentencia, sea cual sea, borraremos todo rastro de esta en las variables
     de la clase por si una misma instancia de esta quisiera seguir ejecutando queries */
    protected clearQuery(){
        this.query=""
        this.values=[]
    }
    protected ifExists(data:string){
        this.query= `SELECT * FROM ${data}`
    }
    /* Con este método obtendremos todas las formas posibles en las que la ejecución de una query puede devolvernos un resultado */
    protected get_result(result:any,option?:string){
        switch (option) {
            case "count":
                return result.rowCount     
            default:
                return result.rows
        }
    }
    protected select(data:select){
        this.query+=`SELECT ${data.columns} FROM ${data.table}`
    }
    protected delete (table_name:any){
        this.query=`DELETE FROM ${table_name}`
    }
    protected insert(tableName:string){
       this.query+=`INSERT INTO ${tableName}`
    }
        /**
        * @params "column,column..."
        */
        protected  colums_insert_into(data:string){
            if(!this.isPosibleToContinue("INSERT INTO")){
                console.error("Is not posible tu create a insert in this sentence:"+this.query)
                return "error"
            }
        this.query+=`(${data})`
        }
        /**
        * @params "value,value..."
        */
        protected  values_insert_into(data:string){
            if(!this.isPosibleToContinue("INSERT INTO") && !this.isPosibleToContinue("(")){
                console.error("Is not posible tu create a insert in this sentence:"+this.query)
                return "error"
            }
            this.query+=` VALUES(${data})`
        }
        /**
        * @params "value,value..."
        */
        protected  more_values_insert_into(data:string){
            if(!this.isPosibleToContinue("VALUES")){
                console.error("Is not posible tu create a insert in this sentence:"+this.query)
                return "error"
            }
            this.query+=` ,(${data})`
        } 
    protected update(table:string){
        this.query=`UPDATE ${table} SET `;
    }
        //update options
        protected update_set(data:any){
            if(!this.isPosibleToContinue(data.previous)){
                console.error("Is not posible tu create a where in this sentence:"+this.query)
                return "error"
            }
            this.query+=`${data.column} = '${data.argument}'`
        }
        protected more_update_set(data:any){
            if(!this.isPosibleToContinue(data.previous)){
                console.error("Is not posible tu create a where in this sentence:"+this.query)
                return "error"
            }
            this.query+=`,${data.column} = '${data.argument}'`
        }

    protected where(data:where_and_or){
       if(!this.isPosibleToContinue(data.previous)){
           console.error("Is not posible tu create a where in this sentence:"+this.query)
           return "error"
       }
       this.query+=` WHERE ${data.column} ${data.condition} $${data.conditionalnumber}`
       this.values.push(data.value)
    }
    protected and(data:where_and_or){
        if(!this.isPosibleToContinue(data.previous)){
            console.error("Is not posible tu create an AND in this sentence:"+this.query)
            return "error"
        }
        this.query+=` AND ${data.column} ${data.condition} $${data.conditionalnumber}`
        this.values.push(data.value)
    }
    protected or(data:where_and_or){
        if(!this.isPosibleToContinue(data.previous)){
            console.error("Is not posible tu create an Or in this sentence:"+this.query)
            return "error"
        }
        this.query+=` OR ${data.column} ${data.condition} $${data.conditionalnumber}`
        this.values.push(data.value)
    }
    protected createTable(data:createTableProps){
        this.query+=`CREATE TABLE ${data.tableName}(` //create
        if(data.valueNames.length != data.valueTypes.length){ //comprueba que hay la misma cantidad de nombres de columnas como tipos
            return "Los Nombres dados no coinciden con el numero de tipos otorgados"
        }
        data.valueNames.forEach((value,index)=>{ //añadimos a la query las cloumnas con sus tipos
            this.query+=` ${value} ${data.valueTypes[index]},`
        })
        this.query+= ` PRIMARY KEY(${data.pk})`
        if (data.fk.create) { //Comporbamos si hay claves foraneas
            for (let index = 0; index < data.fk.valueNames.length; index++) {
                this.query+=`, FOREIGN KEY (${data.fk.valueNames[index]}) 
                              REFERENCES ${data.fk.tableReferences[index]} (${data.fk.referencedValues[index]})`

                
            }
        }
        this.query+=");"

    }
    protected dropTable(data:string){
        this.query= `DROP TABLE ${data}`
    }
   
   /*  
    innerJoin */


}

export default Model


