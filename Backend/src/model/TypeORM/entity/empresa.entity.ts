import {Entity, Column, PrimaryGeneratedColumn} from "typeorm"

@Entity()
export class Empresa {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    nombre:string
    @Column()
    account_type:string
}