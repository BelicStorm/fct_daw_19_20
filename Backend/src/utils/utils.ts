import crypto from "crypto"
import jwt from "jsonwebtoken"
const SECRET_KEY = "test"

/**
* @Description Objeto que almacena todas las funciones comunes de la app
* @namespace Utils
*/
const Utils = {
    encrypt:(text:string)  =>{
        var cipher = crypto.createCipher('aes-256-cbc',text)
        var crypted = cipher.update(text,'utf8','hex')
        crypted += cipher.final('hex');
        return { encryptedData: crypted };
    },
    decrypt:(text:any) =>{ 
        var decipher = crypto.createDecipher('aes-256-cbc', text)
        var dec = decipher.update(text, 'hex', 'utf8')
        dec += decipher.final('utf8');
        return dec;
    },
    //Usando las dos funciones anteriores somos capaces de comparar dos contraseñas cifradas
    compare_cifrate:(password, cifred_password)=>{
        let pass=Utils.encrypt(password).encryptedData  
        if (pass!==cifred_password) {
            return false
        }
        return true

    },
    //generamos el jwt a partir de los datos recibidos
    jwtSign:(data:any)=>{
        return jwt.sign(data,SECRET_KEY,)
    },
    //jwt compare
    jwtCompare:(token:string)=>{
        return new Promise((resolve,reject)=>{
            jwt.verify(token,SECRET_KEY,(err,decoded)=>{
               if (err) {
                   reject(err)
               }
               resolve(decoded);
            })
        })
    }
}

export default Utils